package parametresEx3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

public class Main {
	private Connection connection = null;
	private PreparedStatement st = null;
	private Scanner sc = new Scanner(System.in);

	private void connexioBBDD() throws SQLException {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		connection = DriverManager.getConnection(url, connectionProperties);
		System.out.println("Base de dades connectada!");
	}

	private void disconnect() {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				;
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				;
			}
		}
	}

	public void run() {
		try {
			connexioBBDD();
			paymentsByClient();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			disconnect();
		}
	}

	public String demanarNom() {
		String nomClient;
		System.out.println("Introdueix el nom d'un client: ");
		nomClient = sc.nextLine();
		return nomClient;
	}

	public String demanarCognom() {
		String cognomClient;
		System.out.println("Introdueix seu cognom: ");
		cognomClient = sc.nextLine();
		return cognomClient;
	}

	public String demanarData() {
		int dia, mes, any;
		String data;
		do {
			System.out.println("Introdueix el dia: ");
			dia = sc.nextInt();
			System.out.println("Introdueix el mes: ");
			mes = sc.nextInt();
			System.out.println("Introdueix l'any: ");
			any = sc.nextInt();
		} while (dia < 0 && dia > 31 && mes < 0 && mes > 12 && any < 1900);
		data = String.valueOf(any) + "-" + String.valueOf(mes) + "-" + String.valueOf(dia);
		return data;
	}

	public void paymentsByClient() throws SQLException {
		String nomClient = " ", cognomClient = " ", dataInici = " ", dataFi = " ";
		st = connection.prepareStatement("SELECT payment.amount,payment.payment_date,film.title FROM payment "
				+ "JOIN rental ON payment.rental_id=rental.rental_id "
				+ "JOIN inventory ON rental.inventory_id = inventory.inventory_id "
				+ "JOIN film ON inventory.film_id = film.film_id "
				+ "JOIN customer ON payment.customer_id=customer.customer_id "
				+ "WHERE customer.first_name LIKE ? AND customer.last_name LIKE ? "
				+ "AND payment.payment_date> ?  AND payment.payment_date < ?");
		while (!nomClient.equals("") && !cognomClient.equals("") && !dataInici.equals("0-0-0")
				&& !dataFi.equals("0-0-0")) {
			nomClient = demanarNom();
			cognomClient = demanarCognom();
			System.out.println("Data d'inici: \n");
			dataInici = demanarData();
			System.out.println("Data de fi: \n");
			dataFi = demanarData();
			if (!nomClient.equals("") && !cognomClient.equals("") && !dataInici.equals("0-0-0")
					&& !dataFi.equals("0-0-0")) {
				st.setString(1, nomClient);
				st.setString(2, cognomClient);
				st.setString(3, dataInici);
				st.setString(4, dataFi);
				try (ResultSet rs = st.executeQuery()) {
					System.out.println("Detalls de pagaments del client " + nomClient + " " + cognomClient
							+ "\nEntre la data " + dataInici + " i la data " + dataFi + ".\n");
					while (rs.next()) {
						System.out.println("Quantitat: " + rs.getFloat("payment.amount") + " -- Data de pagament: "
								+ rs.getDate("payment_date") + " -- Pel�licula: " + rs.getString("film.title"));
					}
				}
			}
		}
	}

	public static void main(String[] args) {
		Main ex3Parametres = new Main();
		ex3Parametres.run();
	}
}