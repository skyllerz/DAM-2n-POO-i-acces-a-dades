package transaccionsEx1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

public class Main {

	private Connection connection = null;
	private PreparedStatement st = null;
	private Scanner sc = new Scanner(System.in);

	private void connexioBBDD() throws SQLException {
		String url = "jdbc:mysql://localhost:3306/employees";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		connection = DriverManager.getConnection(url, connectionProperties);
		System.out.println("Base de dades connectada!");
	}

	private void disconnect() {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				;
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				;
			}
		}
	}

	public void run() {
		try {
			connexioBBDD();
			setEmployeeData();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			disconnect();
		}
	}

	public static void main(String[] args) {
		Main transaccionsEx1 = new Main();
		transaccionsEx1.run();
	}

	public String askEmployeeGeneralData(String date) {
		System.out.print("Introdueix el " + date + " de l'empleat: ");
		return sc.nextLine();
	}

	public char askEmployeeGender() {
		char gender;
		do {
			System.out.print("Introdueix el genere de l'empleat (F, M): ");
			gender = sc.nextLine().toUpperCase().charAt(0);
			if (gender != 'M' && gender != 'F')
				System.err.println("Introdueix un genere correcte: F, M");
		} while (gender != 'M' && gender != 'F');
		return gender;
	}

	public String askEmpDept() throws SQLException {
		boolean deptExists = false;
		String dept;
		do {
			System.out.print("Introdueix el nom del departament on es vol assignar l'empleat: ");
			dept = sc.nextLine();
			st = connection.prepareStatement("SELECT dept_name FROM departments WHERE dept_name LIKE ?");
			st.setString(1, dept);
			try (ResultSet rs = st.executeQuery()) {
				while (rs.next()) {
					if (dept.equals(rs.getString("dept_name")))
						deptExists = true;
				}
			}
			if (!deptExists)
				System.err.println("Introdueix un departament que existeixi.");
		} while (!deptExists);
		return dept;
	}

	public int askEmployeeSalary() {
		int salary;
		do {
			System.out.print("Introdueix el sou incial de l'empleat: ");
			salary = sc.nextInt();
			sc.nextLine();
			if (salary < 0)
				System.err.println("Introdueix un sou positiu.");
		} while (salary < 0);
		return salary;
	}

	public void setEmployeeData() throws SQLException {
		String firstName, lastName, title, deptName;
		char gender;
		int salary;
		firstName = askEmployeeGeneralData("nom");
		lastName = askEmployeeGeneralData("cognom");
		gender = askEmployeeGender();
		title = askEmployeeGeneralData("titol");
		deptName = askEmpDept();
		salary = askEmployeeSalary();
		transaction(firstName, lastName, title, deptName, gender, salary);
	}

	public void transaction(String firstName, String lastName, String title, String deptName, char gender, int salary)
			throws SQLException {
		int empNo = 0, files;
		String deptNo;
		String getNextEmpNo = "SELECT MAX(emp_no)+1 FROM employees";
		String getDeptNo = "SELECT dept_no FROM departments WHERE dept_name LIKE ?";
		String insertEmpData = "INSERT INTO employees (emp_no,first_name,last_name,gender,hire_date) VALUES (?,?,?,?,curdate())";
		String insertEmpDept = "INSERT INTO dept_emp (emp_no,dept_no,from_date,to_date) VALUES (?,?,curdate(),'9999-01-01')";
		String insertEmpSalary = "INSERT INTO salaries (emp_no,salary,from_date,to_date) VALUES (?,?,curdate(),'9999-01-01')";
		String insertEmpTitle = "INSERT INTO titles (emp_no,title,from_date,to_date) VALUES (?,?,curdate(),'9999-01-01')";
		connection.setAutoCommit(false);
		try (PreparedStatement getEmpNoSt = connection.prepareStatement(getNextEmpNo);
				PreparedStatement insertEmpDataSt = connection.prepareStatement(insertEmpData);
				PreparedStatement getDeptNoSt = connection.prepareStatement(getDeptNo);
				PreparedStatement insertEmpDeptSt = connection.prepareStatement(insertEmpDept);
				PreparedStatement insertEmpSalarySt = connection.prepareStatement(insertEmpSalary);
				PreparedStatement insertEmpTitleSt = connection.prepareStatement(insertEmpTitle);) {
			try (ResultSet rs = getEmpNoSt.executeQuery()) {
				if (rs.next()) {
					empNo = rs.getInt(1);
					insertEmpDataSt.setInt(1, rs.getInt(1));
					insertEmpDataSt.setString(2, firstName);
					insertEmpDataSt.setString(3, lastName);
					insertEmpDataSt.setString(4, String.valueOf(gender));
					files = insertEmpDataSt.executeUpdate();
					if (files == 1) {
						System.out.println("S'ha insertat l'empleat correctament.");
					} else {
						System.err.println("No s'ha pogut insertar l'empleat.");
					}
				}
			}
			getDeptNoSt.setString(1, deptName);
			try (ResultSet rs = getDeptNoSt.executeQuery()) {
				if (rs.next()) {
					deptNo = rs.getString(1);
					insertEmpDeptSt.setInt(1, empNo);
					insertEmpDeptSt.setString(2, deptNo);
					files = insertEmpDeptSt.executeUpdate();
					if (files == 1) {
						System.out.println("S'ha assignat l'empleat al departament correctament. ");
					} else {
						System.err.println("No s'ha pogut assignar l'empleat al departament.");
					}
				}
			}
			insertEmpSalarySt.setInt(1, empNo);
			insertEmpSalarySt.setInt(2, salary);
			files = insertEmpSalarySt.executeUpdate();
			if (files == 1) {
				System.out.println("S'ha assignat el salari de l'empleat correctament. ");
			} else {
				System.err.println("No s'ha pogut assignar el salari de l'empleat.");
			}
			insertEmpTitleSt.setInt(1, empNo);
			insertEmpTitleSt.setString(2, title);
			files = insertEmpTitleSt.executeUpdate();
			if (files == 1) {
				System.out.println("S'ha assignat el titol de l'empleat correctament. ");
			} else {
				System.err.println("No s'ha pogut assignat el titol de l'empleat.");
			}
			connection.commit();
			System.out.println("Transacci� realitzada!");
		} catch (SQLException e) {
			System.err.print("S'ha produ�t una errada a la transacci�, desfent els canvis...");
			connection.rollback();
		} finally {
			connection.setAutoCommit(true);
		}
	}
}
