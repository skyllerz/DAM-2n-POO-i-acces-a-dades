package parametresEx4;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

public class Main {
	private Connection connection = null;
	private PreparedStatement st = null;
	private Scanner sc = new Scanner(System.in);

	private void connexioBBDD() throws SQLException {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		connection = DriverManager.getConnection(url, connectionProperties);
		System.out.println("Base de dades connectada!");
	}

	private void disconnect() {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				;
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				;
			}
		}
	}

	public void run() {
		try {
			connexioBBDD();
			filmsByWord();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			disconnect();
		}
	}

	public String demanarParaulaClau() {
		String paraulaClau;
		System.out.println("Introdueix el nom de la pel�l�cula: ");
		paraulaClau = sc.nextLine();
		return paraulaClau;
	}

	public void filmsByWord() throws SQLException {
		String paraulaClau = " ";
		st = connection.prepareStatement("SELECT film.title, film.description, address.address FROM film "
				+ "JOIN inventory ON film.film_id=inventory.film_id "
				+ "JOIN store ON store.store_id = inventory.store_id "
				+ "JOIN customer ON store.store_id=customer.store_id "
				+ "JOIN address ON address.address_id = customer.address_id "
				+ "WHERE film.title LIKE ? OR film.description LIKE ? " + "GROUP BY film.title;");
		while (!paraulaClau.equals("")) {
			paraulaClau = demanarParaulaClau();
			if (!paraulaClau.equals("")) {
				st.setString(1, "%" + paraulaClau + "%");
				st.setString(2, "%" + paraulaClau + "%");
				try (ResultSet rs = st.executeQuery()) {
					System.out.println("Pel�l�cules que contenen la paraula " + paraulaClau + ".\n");
					while (rs.next()) {
						System.out.println("Nom : " + rs.getString("film.title") + " -- Descripci�: "
								+ rs.getString("film.description") + " -- Botiga: " + rs.getString("address.address"));
					}
				}
			}
		}
	}

	public static void main(String[] args) {
		Main ex4Parametres = new Main();
		ex4Parametres.run();
	}
}