package parametresEx5;

import java.sql.SQLException;
import java.util.Scanner;

public class Usuari {

	private Scanner sc = new Scanner(System.in);

	public String askFilm() {
		String film;
		System.out.print("\nIntrodueix el nom de la pel�l�cula: ");
		film = sc.nextLine();
		return film;
	}
	
	public String askStore() {
		String store;
		System.out.print("\nIntrodueix el nom de la botiga: ");
		store = sc.nextLine();
		return store;
	}
	
	public void menu() {
		ConnexioBBDD ex5Parametres = new ConnexioBBDD();
		String store = askStore();
		int opcio;
		System.out.println(
				"\n1-Cerca per nom de pel�l�cula\n2-Cerca per nom del actor\n3-Cerca per g�nere de pel�l�cula\n");
		do {
			System.out.print("Introdueix una opci�: ");
			opcio = sc.nextInt();
			sc.nextLine();
			switch (opcio) {
			case 1:
				try {
					ex5Parametres.searchByFilmName(store);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				break;
			case 2:

				break;
			case 3:

				break;
			default:
				break;
			}
		} while (opcio != 0);
	}

	
}
