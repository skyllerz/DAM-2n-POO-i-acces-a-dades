package parametresEx5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class ConnexioBBDD {
	private Connection connection = null;
	private PreparedStatement st2 = null;
	private Statement st = null;
	private Usuari user = new Usuari();

	private void connexioBBDD() throws SQLException {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		connection = DriverManager.getConnection(url, connectionProperties);
		st = connection.createStatement();
		System.out.println("Base de dades connectada!");
	}

	private void disconnect() {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				;
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				;
			}
		}
	}

	public void run() {
		try {

			connexioBBDD();
			showAllStores();
			user.menu();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			disconnect();
		}
	}

	public void showAllStores() throws SQLException {
		try (ResultSet rs = st.executeQuery(
				"SELECT address.address FROM customer " + "JOIN address ON customer.address_id=address.address_id "
						+ "JOIN store ON store.store_id=customer.store_id " + "GROUP BY address.address")) {
			System.out.println("Botigues: \n");
			while (rs.next()) {
				System.out.println(rs.getString("address.address"));
			}
		}
	}

	public void searchByFilmName(String store) throws SQLException {
		String film = user.askFilm();
		st2 = connection.prepareStatement(
				"SELECT film.title,address.address FROM store " + "JOIN customer ON customer.store_id=store.store_id "
						+ "JOIN address ON customer.address_id=address.address_id "
						+ "JOIN inventory ON store.store_id=inventory.store_id "
						+ "JOIN film ON inventory.film_id=film.film_id "
						+ "WHERE address.address LIKE ? AND film.title LIKE ? GROUP BY film.title");
		while (!film.equals("") && !store.equals("")) {
			film = user.askFilm();
			if (!film.equals("") && !store.equals("")) {
				System.out.println(film + " " + store);
				st2.setString(1, store);
				st2.setString(2, "%" + film + "%");
				try (ResultSet rs = st2.executeQuery()) {
					System.out
							.println("Pel�l�cules que contenen la paraula " + film + " de la botiga " + store + ".\n");
					while (rs.next()) {
						System.out.println("Nom : " + rs.getString("film.title") + " -- Botiga: "
								+ rs.getString("address.address"));
					}
				}
			}
		}
	}

	public static void main(String[] args) {
		ConnexioBBDD ex5Parametres = new ConnexioBBDD();
		ex5Parametres.run();
	}
}