package modificacioEx1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		if (args.length == 1) {
			StringBuilder consultaOpcio3 = new StringBuilder();
			consultaOpcio3.append("INSERT INTO noms2012 VALUES ");
			Main main = new Main();
			String s;
			String[] s2;
			int opcio, files;
			long tempsInicial, tempsFinal;
			Scanner sc = new Scanner(System.in);
			System.out.println(
					"Introdueix una opci�:\n1- Inserir per Statment\n2- Inserir per PreparedStatement\n3- Inserir per unica consulta SQL\n4- Esborrar Taula");
			opcio = sc.nextInt();
			try (BufferedReader lector = new BufferedReader(new FileReader(args[0]))) {
				files = 0;
				tempsInicial = System.currentTimeMillis();
				while ((s = lector.readLine()) != null && opcio > 0 && opcio < 4) {
					s2 = s.split(";");
					if (opcio == 1) {
						files += main.inserirPerStatment(s2[0], s2[1], s2[2], s2[3], s2[4]);
					} else if (opcio == 2) {
						files += main.inserirPerPreparedStatment(s2[0], s2[1], s2[2], s2[3], s2[4]);
					} else if (opcio == 3) {
						consultaOpcio3
								.append("("
										+ s2[0] + ",'" + s2[1] + "','" + s2[2] + "'," + s2[3] + ",'" + s2[4] + "'), ");
						files++;
					}
				}
				consultaOpcio3.setCharAt(consultaOpcio3.length()-2, ';');
				tempsFinal = System.currentTimeMillis();
				if (opcio >=1 && opcio<=3)
					System.out.println("Registres inserits: " + files);
				if (opcio == 3)
					main.inserirTotDeCop(consultaOpcio3);
				else if (opcio == 4) {
					main.esborrarTaula();
					System.out.println("Taula Buidada.");
				}
				System.out.println("Temps Trigat: " + (tempsFinal - tempsInicial) + "ms.");
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
			sc.close();
		} else {
			System.err.println("Introdueix un sol parametre.");
		}
	}

	public int inserirPerStatment(String posicio, String nom, String sexe, String frequencia, String percentatge) {
		String url = "jdbc:mysql://localhost:3306/onomastica";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		int files = 0;
		try (Connection con = DriverManager.getConnection(url, connectionProperties);
				Statement st = con.createStatement()) {
			files += st.executeUpdate(
					"INSERT INTO " + "noms2012(posicio, nom, sexe, frequencia, percentatge) " + "VALUES('" + posicio
							+ "','" + nom + "','" + sexe + "','" + frequencia + "','" + percentatge + "')");
		} catch (SQLException e) {
			System.err.println("Error SQL: " + e.getMessage());
		}
		return files;
	}

	public int inserirPerPreparedStatment(String posicio, String nom, String sexe, String frequencia,
			String percentatge) {
		int files = 0;
		try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/onomastica", "root", "");
				PreparedStatement inserirNomsNascuts = connection.prepareStatement(
						"INSERT INTO noms2012(posicio, nom, sexe, frequencia, percentatge) VALUES (?,?,?,?,?)");) {
			inserirNomsNascuts.setString(1, posicio);
			inserirNomsNascuts.setString(2, nom);
			inserirNomsNascuts.setString(3, sexe);
			inserirNomsNascuts.setString(4, frequencia);
			inserirNomsNascuts.setString(5, percentatge);
			files += inserirNomsNascuts.executeUpdate();
		} catch (SQLException e) {
			// Missatge retornat pel SGBD
			System.err.println("Missatge: " + e.getMessage());
			// Codi d'error est�ndard
			System.err.println("Estat SQL: " + e.getSQLState());
			// Codi d'error propi del fabricant del SGBD
			System.err.println("Codi de l'error: " + e.getErrorCode());
		}
		return files;
	}

	public void inserirTotDeCop(StringBuilder cadena) {
		String url = "jdbc:mysql://localhost:3306/onomastica";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		try (Connection con = DriverManager.getConnection(url, connectionProperties);
				Statement st = con.createStatement()) {
			st.executeQuery(cadena.toString());
		} catch (SQLException e) {
			System.err.println("Error SQL: " + e.getMessage());
		}
	}

	public void esborrarTaula() {
		String url = "jdbc:mysql://localhost:3306/onomastica";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		try (Connection con = DriverManager.getConnection(url, connectionProperties);
				Statement st = con.createStatement()) {
			st.executeUpdate("TRUNCATE TABLE noms2012");
		} catch (SQLException e) {
			System.err.println("Error SQL: " + e.getMessage());
		}
	}

}
