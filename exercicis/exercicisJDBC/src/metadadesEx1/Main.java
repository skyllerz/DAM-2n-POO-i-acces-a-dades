package metadadesEx1;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		DatabaseMetaData dbmd = null;
		Scanner sc = new Scanner(System.in);
		try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/sakila", "root", "");) {
			dbmd = connection.getMetaData();
			System.out.print("Introdueix el nom de la taula: ");
			String nomTaula = sc.nextLine();
			primaryKeysByTable(dbmd, nomTaula);
			importedPrimaryKeysByTable(dbmd, nomTaula);
			exportedPrimaryKeysByTable(dbmd, nomTaula);
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		sc.close();
	}

	public static void primaryKeysByTable(DatabaseMetaData dbmd, String nomTaula) throws SQLException {
		try (ResultSet rs = dbmd.getPrimaryKeys(null, null, nomTaula);) {
			System.out.println("\nClaus primaries de la taula: " + nomTaula + "\n");
			while (rs.next()) {
				String primaryKey = rs.getString(4);
				System.out.println(primaryKey);
			}
			System.out.println();
		}
	}

	public static void importedPrimaryKeysByTable(DatabaseMetaData dbmd, String nomTaula) throws SQLException {
		try (ResultSet rs = dbmd.getImportedKeys(null, null, nomTaula);) {
			System.out.println("Claus primaries importades per la taula " + nomTaula + "\n");
			while (rs.next()) {
				String pkTableName = rs.getString(3);
				String primaryKey = rs.getString(4);
				String fkTableName = rs.getString(7);
				String foriegnKey = rs.getString(8);
				System.out.println(" "+fkTableName + "." + foriegnKey + " --> " + pkTableName + "." + primaryKey);
			}
			System.out.println();
		}
	}

	public static void exportedPrimaryKeysByTable(DatabaseMetaData dbmd, String nomTaula) throws SQLException {
		try (ResultSet rs = dbmd.getExportedKeys(null, null, nomTaula);) {
			System.out.println("Claus primaries exportades per la taula " + nomTaula + "\n");
			while (rs.next()) {
				String pkTableName = rs.getString(3);
				String primaryKey = rs.getString(4);
				String fkTableName = rs.getString(7);
				String foriegnKey = rs.getString(8);
				System.out.println(" "+pkTableName + "." + primaryKey + " --> " + fkTableName + "." + foriegnKey);
			}
			System.out.println();
		}
	}
}
