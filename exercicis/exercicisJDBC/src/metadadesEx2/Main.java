package metadadesEx2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		ResultSetMetaData rsmd = null;
		Scanner sc = new Scanner(System.in);
		try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/sakila", "root", "");) {
			System.out.print("Introdueix la sentencia SQL: ");
			String sentenciaSQL = sc.nextLine();
			sqlSentence(rsmd, sentenciaSQL);

		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		sc.close();
	}

	public static void sqlSentence(ResultSetMetaData rsmd, String sentenciaSQL) throws SQLException {
		try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/sakila", "root", "");
				Statement st = connection.createStatement();
				ResultSet rs = st.executeQuery(sentenciaSQL);) {
			rsmd = rs.getMetaData();
			int nCols = rsmd.getColumnCount();
			for (int i = 1; i <= nCols; i++) {
				System.out.print(rsmd.getColumnName(i) + "\t");
			}
			System.out.println("\n");
			while (rs.next()) {
				for (int i = 1; i <= nCols; i++) {
					System.out.print(rs.getString(i) + "\t");
				}
				System.out.println();
			}
		}
	}
}
