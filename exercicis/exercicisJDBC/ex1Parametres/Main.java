package ex1Parametres;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

public class Main {
	private Connection connection = null;
	private PreparedStatement st = null;
	private Scanner sc = new Scanner(System.in);
	
	private void connexioBBDD() throws SQLException {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		connection = DriverManager.getConnection(url, connectionProperties);
		System.out.println("Base de dades connectada!");
	}

	private void disconnect() {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				;
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				;
			}
		}
	}

	public void run() {
		try {
			connexioBBDD();
			filmsByLastNameActor();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			disconnect();
		}
	}

	public String demanarCognom() {
		String cognomActor;
		System.out.println("Introdueix el cognom d'un actor: ");
		cognomActor = sc.nextLine();
		return cognomActor;
	}

	public void filmsByLastNameActor() throws SQLException {
		String cognomActor = " ";
		st = connection.prepareStatement(
				"SELECT film.title from actor " + "JOIN film_actor ON actor.actor_id=film_actor.actor_id "
						+ "JOIN film ON film_actor.film_id=film.film_id WHERE last_name LIKE ?");
		while (!cognomActor.equals("")) {
			cognomActor = demanarCognom();
			if (!cognomActor.equals("")) {
				st.setString(1, cognomActor);
				try (ResultSet rs = st.executeQuery()) {
					System.out.println("Pelicules del Actor "+cognomActor+":\n");
					while (rs.next()) {
						System.out.println("Pelicula: " + rs.getString("film.title"));
					}
				}
			}
		}
	}

	public static void main(String[] args) {
		Main exercici2 = new Main();
		exercici2.run();
	}
}