package exercici6;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Main {
	private Connection connection = null;
	private Statement st = null;

	private void connexioBBDD() throws SQLException {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		connection = DriverManager.getConnection(url, connectionProperties);
		st = connection.createStatement();
		System.out.println("Base de dades connectada!");
	}

	private void disconnect() {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				;
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				;
			}
		}
	}

	public void run() {
		try {
			connexioBBDD();
			filmsByCategory();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			disconnect();
		}
	}

	public void filmsByCategory() throws SQLException {
		
		try (ResultSet rs = st.executeQuery(
				"SELECT category.name, count(film.title) as num_pelicules FROM film_category "
						+ "JOIN film ON film_category.film_id=film.film_id "
						+ "JOIN category ON film_category.category_id=category.category_id "
						+ "GROUP BY category.name ORDER BY category.name")) {
			System.out.println("Categories i numero de pel�licules de cadascuna:\n");
			while (rs.next()) {
				System.out.println("Categoria: "+ rs.getString("category.name") 
				+" -- Num de pel�licules: "+ rs.getString("num_pelicules"));
			}
		}

	}

	public static void main(String[] args) {
		Main exercici2 = new Main();
		exercici2.run();
	}
}