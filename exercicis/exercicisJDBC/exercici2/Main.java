package exercici2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Main {
	private Connection connection = null;
	private Statement st = null;

	private void connexioBBDD() throws SQLException {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		connection = DriverManager.getConnection(url, connectionProperties);
		st = connection.createStatement();
		System.out.println("Base de dades connectada!");
	}

	private void disconnect() {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				;
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				;
			}
		}
	}

	public void run() {
		try {
			connexioBBDD();
			actorsByFilm();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			disconnect();
		}
	}

	public void actorsByFilm() throws SQLException {
		String pelicula = "TWISTED PIRATES";
		try (ResultSet rs = st.executeQuery("SELECT first_name, last_name "
				+ "FROM actor JOIN film_actor ON actor.actor_id=film_actor.actor_id "
				+ "JOIN film on film_actor.film_id=film.film_id  WHERE title LIKE '"
				+ pelicula + "'")) {
			System.out.println("Noms i cognoms de actors que surten a la pel�licula "+pelicula);
			while (rs.next()) {
				System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
			}
		}
	}

	public static void main(String[] args) {
		Main exercici2 = new Main();
		exercici2.run();
	}
}