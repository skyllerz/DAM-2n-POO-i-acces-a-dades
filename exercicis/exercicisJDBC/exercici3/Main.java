package exercici3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Main {
	private Connection connection = null;
	private Statement st = null;

	private void connexioBBDD() throws SQLException {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		connection = DriverManager.getConnection(url, connectionProperties);
		st = connection.createStatement();
		System.out.println("Base de dades connectada!");
	}

	private void disconnect() {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				;
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				;
			}
		}
	}

	public void run() {
		try {
			connexioBBDD();
			itemsShopsByClient();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			disconnect();
		}
	}

	public void itemsShopsByClient() throws SQLException {

		String first_name = "ALLISON";
		String last_name = "STANLEY";
		try (ResultSet rs = st.executeQuery(
				"SELECT address.address, film.title, film.rental_duration, rental.return_date, DATE_ADD(rental.rental_date,INTERVAL film.rental_duration DAY) AS actual_rental_date FROM inventory "
						+ "JOIN store ON inventory.store_id=store.store_id "
						+ "JOIN rental ON inventory.inventory_id=rental.inventory_id "
						+ "JOIN film ON inventory.film_id=film.film_id "
						+ "JOIN customer ON rental.customer_id=customer.customer_id "
						+ "JOIN address ON customer.address_id=address.address_id " + "WHERE customer.first_name LIKE '"
						+ first_name + "' AND customer.last_name LIKE '" + last_name
						+ "' AND return_date IS NULL ORDER BY film.title")) {
			System.out.println("Items i quines tendes tenen aquests items del client " + first_name + " " + last_name);
			while (rs.next()) {
				System.out.println("Adre�a: " + rs.getString("address.address") + " -- Pel�l�cula: "
						+ rs.getString("film.title") + " -- Data que s'ha d'entregar: " + rs.getString("actual_rental_date"));
			}
		}

	}

	public static void main(String[] args) {
		Main exercici2 = new Main();
		exercici2.run();
	}
}