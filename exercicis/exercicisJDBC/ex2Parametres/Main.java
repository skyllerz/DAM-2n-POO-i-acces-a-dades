package ex2Parametres;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

public class Main {
	private Connection connection = null;
	private PreparedStatement st = null;
	private Scanner sc = new Scanner(System.in);
	
	private void connexioBBDD() throws SQLException {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		connection = DriverManager.getConnection(url, connectionProperties);
		System.out.println("Base de dades connectada!");
	}

	private void disconnect() {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				;
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				;
			}
		}
	}

	public void run() {
		try {
			connexioBBDD();
			itemsShopsByClient();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			disconnect();
		}
	}
	
	public String demanarNom() {
		String nomClient;
		System.out.println("Introdueix el nom d'un client: ");
		nomClient = sc.nextLine();
		return nomClient;
	}
	
	public String demanarCognom() {
		String cognomClient;
		System.out.println("Introdueix seu cognom: ");
		cognomClient = sc.nextLine();
		return cognomClient;
	}

	public void itemsShopsByClient() throws SQLException {
		String nomClient = " ", cognomClient = " ";
		st = connection.prepareStatement(
				"SELECT address.address, film.title, film.rental_duration, rental.return_date, DATE_ADD(rental.rental_date,INTERVAL film.rental_duration DAY) AS actual_rental_date FROM inventory "
						+ "JOIN store ON inventory.store_id=store.store_id "
						+ "JOIN rental ON inventory.inventory_id=rental.inventory_id "
						+ "JOIN film ON inventory.film_id=film.film_id "
						+ "JOIN customer ON rental.customer_id=customer.customer_id "
						+ "JOIN address ON customer.address_id=address.address_id " 
						+ "WHERE customer.first_name LIKE ? AND customer.last_name LIKE ? AND return_date IS NULL ORDER BY film.title");
		while (!nomClient.equals("") && !cognomClient.equals("")) {
			nomClient = demanarNom();
			cognomClient = demanarCognom();
			if (!nomClient.equals("") && !cognomClient.equals("")) {
				st.setString(1, nomClient);
				st.setString(2, cognomClient);
				try (ResultSet rs = st.executeQuery()) {
					System.out.println("Items i quines tendes tenen aquests items del client " + nomClient + " " + cognomClient);
					while (rs.next()) {
						System.out.println("Adre�a: " + rs.getString("address.address") + " -- Pel�l�cula: "
								+ rs.getString("film.title") + " -- Data que s'ha d'entregar: " + rs.getString("actual_rental_date"));
					}
				}
			}
		}
	}

	public static void main(String[] args) {
		Main exercici2 = new Main();
		exercici2.run();
	}
}