package exercici4;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Main {
	private Connection connection = null;
	private Statement st = null;

	private void connexioBBDD() throws SQLException {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		connection = DriverManager.getConnection(url, connectionProperties);
		st = connection.createStatement();
		System.out.println("Base de dades connectada!");
	}

	private void disconnect() {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				;
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				;
			}
		}
	}

	public void run() {
		try {
			connexioBBDD();
			filmsByHour();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			disconnect();
		}
	}

	public void filmsByHour() throws SQLException {
		try (ResultSet rs = st.executeQuery("SELECT address.address, staff.first_name, staff.last_name "
				+ "FROM staff JOIN address ON staff.address_id=address.address_id "
				+ "JOIN store ON store.store_id=staff.store_id")) {
			System.out.println("Adre�a de totes les botigues, amb el nom i el cognom de l'encarregat.");
			while (rs.next()) {
				System.out.println("Adre�a: " +rs.getString("address.address") + " -- Encarregat: " + rs.getString("staff.first_name") + " "
						+ rs.getString("staff.last_name"));
			}
		}
	}

	public static void main(String[] args) {
		Main exercici2 = new Main();
		exercici2.run();
	}
}