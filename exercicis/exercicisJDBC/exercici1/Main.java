package exercici1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Main {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		try (Connection con = DriverManager.getConnection(url,connectionProperties);
				Statement st = con.createStatement();) {
			System.out.println("Base de dades connectada!");
			try (ResultSet rs = st.executeQuery("SELECT * FROM film ORDER BY title, description")) {
				System.out.println("Nom i descripci� de totes les pel�licules de la taula film:");
				while (rs.next()) {
					int length = rs.getInt("length");
					if (length>150){
						String title = rs.getString("title");
						String description = rs.getString("description");
						System.out.println(title+"\t"+description);
					}
				}
			}
		} catch (SQLException e) {
			System.err.println("Error SQL: " + e.getMessage());
		}
	}
}