package exercici5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Main {
	private Connection connection = null;
	private Statement st = null;

	private void connexioBBDD() throws SQLException {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		connection = DriverManager.getConnection(url, connectionProperties);
		st = connection.createStatement();
		System.out.println("Base de dades connectada!");
	}

	private void disconnect() {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				;
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				;
			}
		}
	}

	public void run() {
		try {
			connexioBBDD();
			infoClients();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			disconnect();
		}
	}

	public void infoClients() throws SQLException {
		
		try (ResultSet rs = st.executeQuery(
				"SELECT customer.first_name, customer.last_name, film.title, film.rental_duration, rental.rental_date FROM inventory "
						+ "JOIN rental ON inventory.inventory_id=rental.inventory_id "
						+ "JOIN film ON inventory.film_id=film.film_id "
						+ "JOIN customer ON rental.customer_id=customer.customer_id "
						+ "WHERE return_date IS NULL AND DATE_ADD(rental.rental_date,INTERVAL film.rental_duration DAY) < NOW() ORDER BY film.title")) {
			System.out.println("Nom, cognom i titol de la pel�licula que han de retornar ");
			while (rs.next()) {
				System.out.println("Nom: "+ rs.getString("customer.first_name") 
				+" -- Cognom: "+ rs.getString("customer.last_name")
				+ " -- Pel�l�cula: "+ rs.getString("film.title"));
			}
		}

	}

	public static void main(String[] args) {
		Main exercici2 = new Main();
		exercici2.run();
	}
}