package representacioDocuments;

public class Actor {
	private int actorId;
	private String lastname,firstname;
	public Actor(int actorId,String lastname,String firstname){
		this.actorId=actorId;
		this.lastname=lastname;
		this.firstname=firstname;
	}
	public int getActorId() {
		return actorId;
	}
	public void setActorId(int actorId) {
		this.actorId = actorId;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastName) {
		this.lastname = lastName;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
}
