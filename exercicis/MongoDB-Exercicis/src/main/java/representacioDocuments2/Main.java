package representacioDocuments2;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.Document;
import org.bson.json.JsonWriterSettings;

public class Main {

	public static void main(String[] args) {
		List<Rental> rentals = new ArrayList<Rental>();
		
		List<Document> rentalsDoc = new ArrayList<Document>();
		
		rentals.add(new Rental(1185,"2005-06-15 00:54:12.0", "2005-06-24 02:42:12.0", 2, 611, "MUSKETEERS WAIT", new Payment(3, 6.00,"2005-06-15 00:54:12.0")));
		rentals.add(new Rental(1476, "2005-06-15 21:08:46.0", "2005-06-25 02:26:46.0", 1, 308, "FERRIS MOTHER", new Payment (5, 10.00, "2005-06-15 21:08:46.0")));
		rentals.add(new Rental(1725, "2005-06-16 15:18:57.0", "2005-06-17 21:05:57.0", 1, 159, "CLOSER BANG", new Payment (6, 5.00, "2005-06-16 15:18:57.0")));

		
		for(Rental rental:rentals){

			Document document = new Document()
					.append("Payment Id", rental.getPayment().getPaymentId())
					.append("Amount", rental.getPayment().getAmount())
					.append("Payment Date", rental.getPayment().getPaymentDate());
			
			Document document2 = new Document()
					.append("rentalId", rental.getRentalId())
					.append("Rental Date", rental.getRentalDate())
					.append("Return Date", rental.getReturnDate())
					.append("staffId", rental.getStaffId())
					.append("filmId", rental.getFilmId())
					.append("Film Title", rental.getFilmTitle())
					.append("Payments", Arrays.asList(document));
			rentalsDoc.add(document2);

		}
		
		Document document = new Document()
				.append("_id", 1)
				.append("First Name", "MARY")
				.append("Last Name","SMITH")
				.append("Address", "1913 Hanoi Way")
				.append("District", "Nagasaki")
				.append("City", "Sasebo")
				.append("Country", "Japan")
				.append("Phone", "28303384290")
				.append("Rentals", rentalsDoc);

		JsonWriterSettings settings = new JsonWriterSettings(true);
		System.out.println(document.toJson(settings));
		
	}

}
