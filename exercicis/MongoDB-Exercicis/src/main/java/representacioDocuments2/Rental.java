package representacioDocuments2;

public class Rental {
	private int rentalId;
	private String rentalDate;
	private String returnDate;
	private int staffId;
	private int filmId;
	private String filmTitle;
	private Payment payment;
	public Rental(int rentalId, String rentalDate, String returnDate, int staffId, int filmId,
			String filmTitle, Payment payment) {
		this.rentalId = rentalId;
		this.rentalDate = rentalDate;
		this.returnDate = returnDate;
		this.staffId = staffId;
		this.filmId = filmId;
		this.filmTitle = filmTitle;
		this.payment = payment;
	}
	public int getRentalId() {
		return rentalId;
	}
	public void setRentalId(int rentalId) {
		this.rentalId = rentalId;
	}
	public String getRentalDate() {
		return rentalDate;
	}
	public void setRentalDate(String rentalDate) {
		this.rentalDate = rentalDate;
	}
	public String getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(String
			returnDate) {
		this.returnDate = returnDate;
	}
	public int getStaffId() {
		return staffId;
	}
	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}
	public int getFilmId() {
		return filmId;
	}
	public void setFilmId(int filmId) {
		this.filmId = filmId;
	}
	public String getFilmTitle() {
		return filmTitle;
	}
	public void setFilmTitle(String filmTitle) {
		this.filmTitle = filmTitle;
	}
	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
	
}
