package exerciciFitxers;

import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CopiaFitxer {

	public static void main(String[] args) {

		if (args.length == 2) {
			Path fit = Paths.get(args[0]);
			Path dir = Paths.get(args[1]);
			System.out.println("Copiant el fitxer "+fit.getFileName()+ " al directori " + dir+"...");
			try {
				Files.copy(fit,dir);
			} catch (IOException | DirectoryIteratorException ex) {
				System.err.println(ex);
			}
			System.out.println("Fitxer Copiat Correctament");

		}

	}

}
