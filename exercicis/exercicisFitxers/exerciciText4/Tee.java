package exerciciText4;

import java.io.FileWriter;
import java.io.IOException;

public class Tee {

	public static void main(String[] args) {

		if (args.length == 1) {
			String frase = "Frase exercici de text 4";
			String fitxer = args[0];
			char caracter;
			try (FileWriter lector = new FileWriter(fitxer)) {
				for (int i = 0; i < frase.length(); i++) {
					caracter = frase.charAt(i);
					System.out.println(caracter);
					lector.append(caracter);
				}
			} catch (IOException e) {
				System.err.println(e);
			}
		}
	}

}
