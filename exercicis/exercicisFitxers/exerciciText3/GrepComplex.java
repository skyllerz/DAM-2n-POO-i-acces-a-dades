package exerciciText3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class GrepComplex {

	private String paraula;
	private Path dir;
	private List<RegistreFitxer> registre;

	public GrepComplex(String paraula, Path dir) {
		super();
		this.paraula = paraula;
		this.dir = dir;
		this.registre = new ArrayList<RegistreFitxer>();
	}

	public static void main(String[] args) {
		GrepComplex grep = new GrepComplex(args[0], Paths.get(args[1]));

		if (args.length == 2) {
			if (grep.isDirectory()) {
				grep.carregarDirectoris();
				System.out.println(grep);
			} else if (grep.isRegularFile()) {
				grep.carregarFitxer();
			}
			
		} else {
			System.err.println("Utilització: java grepComplex <paraula> <directori>/<fitxer>");
		}
	}

	public boolean isDirectory() {
		return Files.isDirectory(dir);
	}

	public boolean isRegularFile() {
		return Files.isRegularFile(dir);
	}

	private void carregarDirectoris() {
		String s;
		int contador;
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
			for (Path fitxer : stream) {
				if (Files.isRegularFile(fitxer)) {
					try (BufferedReader lector = new BufferedReader(new FileReader(fitxer.toString()))) {
						contador = 0;
						RegistreFitxer registre = new RegistreFitxer(fitxer.getFileName().toString());
						while ((s = lector.readLine()) != null) {
							contador++;
							if (s.contains(paraula)) {
								registre.addFila(contador);
							}
						}
						if (!registre.isEmpty()) {
							this.registre.add(registre);
						}
					} catch (IOException e) {
						System.err.println(e.getMessage());
					}
				}
			}
		} catch (IOException | DirectoryIteratorException ex) {
			System.err.println(ex);
		}
	}

	@Override
	public String toString() {
		String s = "La paraula " + paraula + " apareix en els seguents fitxers: \n";
		for (RegistreFitxer fitxer : this.registre) {
			s+="\tFitxer "+fitxer.getNom()+":\n";
			List<Integer> fila = fitxer.getFila();
			for (Integer integer : fila) {
				s+="\t\tFila "+integer;
			}
			s+="\n";
		}
		return s;
	}

	public void carregarFitxer(){
		int contador;
		String s;
		try (BufferedReader lector = new BufferedReader(new FileReader(dir.toString()))) {
			contador = 0;
			System.out.println("La paraula " + paraula + " apareix en les seguents linies: \n");
			System.out.println("Fitxer "+dir.toString()+": ");
			while ((s = lector.readLine()) != null) {
				contador++;
				if (s.contains(paraula)) {
					System.out.println("Fila "+contador);
				}
			}

		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}
