package exerciciText3;

import java.util.ArrayList;
import java.util.List;

public class RegistreFitxer {
	private String nom;
	private List<Integer> fila;

	public RegistreFitxer(String nom) {
		super();
		this.nom = nom;
		this.fila = new ArrayList<Integer>();
	}

	public void addFila(int num) {
		fila.add(num);
	}

	public String getNom() {
		return nom;
	}

	public List<Integer> getFila() {
		return fila;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setFila(List<Integer> fila) {
		this.fila = fila;
	}

	public boolean isEmpty() {

		return fila.isEmpty();
	}
}
