package exerciciXML1;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.io.FileWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class LlegirPersona {

	private List<Persona> persones = new ArrayList<Persona>();

	public static void main(String[] args) {
		LlegirPersona p = new LlegirPersona();
		p.guardarPersones(p.persones);
		p.llegirPersones(p.persones);
	}

	public void guardarPersones(List<Persona> persones) {
		try (ObjectInputStream lector = new ObjectInputStream(new FileInputStream("persones.bin"));) {
			while (true) {
				Object o = lector.readObject();
				if (o instanceof Persona) {
					persones.add((Persona) o);
				}
			}
		} catch (EOFException ex) {
			System.out.println("S'ha llegit el fitxer sencer.");
		} catch (IOException ex) {
			System.err.println(ex);
		} catch (ClassNotFoundException ex) {
			System.err.println(ex);
		}
	}

	public void llegirPersones(List<Persona> persones) {
		try (FileWriter writer = new FileWriter("persones.xml")) {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = builder.newDocument();
			document.setXmlVersion("1.0");
			System.out.println("\nLlegint el fitxer XML:\n");
			Element elementPersones = document.createElement("persones");
			document.appendChild(elementPersones);
			for (Persona persona : persones) {
				Element elementPersona = document.createElement("persona");
				document.getDocumentElement().appendChild(elementPersona);

				Element elementNom = document.createElement("nom");
				Text text = document.createTextNode(persona.getNom());
				elementNom.appendChild(text);
				elementPersona.appendChild(elementNom);

				Element elementEdat = document.createElement("edat");
				Text text2 = document.createTextNode(String.valueOf(persona.getEdat()));
				elementEdat.appendChild(text2);
				elementPersona.appendChild(elementEdat);
			}
			Source source = new DOMSource(document);
			Result result = new StreamResult(writer);

			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xalan}indent-amount", "5");
			transformer.transform(source, result);

			Result console = new StreamResult(System.out);
			transformer.transform(source, console);

		} catch (ParserConfigurationException | TransformerException | IOException e) {
			System.err.println(e.getMessage());
		}
	}

}