package exerciciXML1;

import java.io.Serializable;

public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	private String nom;
	private int edat;

	public Persona(String nom, int edat) {
		this.nom = nom;
		this.edat = edat;
	}

	public String getNom() {
		return nom;
	}

	public int getEdat() {
		return edat;
	}
	public String toString(){
		return nom+" "+edat;
		
	}
	

}
