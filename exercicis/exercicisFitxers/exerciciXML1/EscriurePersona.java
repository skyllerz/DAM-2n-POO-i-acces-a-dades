package exerciciXML1;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class EscriurePersona {
	public static void main(String[] args) {
		Persona[] persones = new Persona[3];
		persones[0] = new Persona("Dani", 20);
		persones[1] = new Persona("Maria", 24);
		persones[2] = new Persona("Pepe", 22);
		try (ObjectOutputStream escriptor = new ObjectOutputStream(new FileOutputStream("persones.bin"));) {
			for (Persona persona : persones) {
				escriptor.writeObject(persona);
				/*escriptor.writeChars(persona.getNom());
				escriptor.writeInt(persona.getEdat());*/
			}
		} catch (IOException ex) {
			System.err.println(ex);
		}
	}

}
