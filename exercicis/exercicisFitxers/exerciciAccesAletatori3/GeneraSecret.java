package exerciciAccesAletatori3;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class GeneraSecret {

	public static Random rnd = new Random();

	public static void main(String[] args) {

		int numAleatori;
		String secret;
		try (DataOutputStream escriptor = new DataOutputStream(new FileOutputStream("dades.bin"))) {
			// Creaci� del primer n�mero aleatori
			numAleatori = rnd.nextInt(3) + 1;
			secret = creaSecret();
			// Print per veure la sortida de la parella
			// System.out.println(numAleatori+" - "+secret);

			//Escrivim la combinaci� al fitxer binari
			escriptor.writeInt(numAleatori);
			escriptor.writeChars(secret);
			for (int j = 0; j < 1000; j++) {
				// Creaci� dels seg�ents n�meros aleatoris
				numAleatori = numAleatori + rnd.nextInt(3) + 1;
				secret = creaSecret();
				// Print per veure la sortida de les parelles
				// System.out.println(numAleatori+" - "+secret);
				
				//Escrivim cadascuna de les combinacions al fitxer binari
				escriptor.writeInt(numAleatori);
				escriptor.writeChars(secret);
			}

		} catch (FileNotFoundException e) {
			System.err.println(e);
		} catch (IOException e) {
			System.err.println(e);
		}
	}

	// Aquest metode ens crea la cadena de 3 caracters aleatoris.

	public static String creaSecret() {
		String secret = "";
		int caracterAleatori;
		for (int i = 0; i < 3; i++) {
			caracterAleatori = rnd.nextInt(26) + 97;
			secret += String.valueOf(Character.toChars(caracterAleatori));
		}
		return secret;
	}

}
