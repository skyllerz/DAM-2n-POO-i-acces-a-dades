package exerciciAccesAletatori3;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class CercaSecret {

	public static void main(String[] args) {
		int codi;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix un codi a cercar: ");
		codi = sc.nextInt();
		sc.nextLine();
		cercaSecret(codi);
		sc.close();
	}

	public static void cercaSecret(int codi) {
		int codiFitxer = 0;
		String secret;
		try (RandomAccessFile fitxer = new RandomAccessFile("dades.bin", "r")) {
			int minExtrem = 0;
			int maxExtrem = (int) fitxer.length() / 10;
			int pos = ((minExtrem + maxExtrem) / 2) * 10;
			boolean numTrobat = false;

			while (minExtrem <= maxExtrem && !numTrobat) {
				fitxer.seek(pos);
				codiFitxer = fitxer.readInt();
				if (codi > codiFitxer) {
					minExtrem = (pos / 10) + 1;
				} else if (codi < codiFitxer) {
					maxExtrem = (pos / 10) - 1;
				} else if (codi == codiFitxer) {
					numTrobat = true;
				}
				pos = ((minExtrem + maxExtrem) / 2) * 10;
			}
			if (numTrobat) {
				fitxer.seek(pos + 4);
				secret = readChars(fitxer, 3);
				System.out.println("Secret del codi: " + codiFitxer + " -> " + secret);
			} else {
				System.err.println("No s'ha trobat el codi.");
			}
		} catch (IOException e) {
			System.err.println(e);
		}
	}

	private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
		StringBuilder b = new StringBuilder();
		char ch = ' ';
		for (int i = 0; i < nChars; i++) {
			ch = fitxer.readChar();
			if (ch != '\0')
				b.append(ch);
		}
		return b.toString();
	}
}
