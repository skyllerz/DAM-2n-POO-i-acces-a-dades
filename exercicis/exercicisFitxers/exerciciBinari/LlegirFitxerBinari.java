
// He creat una classe adicional per poder llegir el contingut de l'exercici EscriptorBytes.java

package exerciciFitxers;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class LlegirFitxerBinari {

	public static void main(String[] args) {
		char[] secret = new char[3];
		String resultat="";
		try (DataInputStream lector = new DataInputStream(new FileInputStream("dades.bin"))) {
			while (true) {
				resultat += lector.readInt()+" - ";
				for (int i = 0; i < 3; i++)
					secret[i] = lector.readChar();
				resultat+=String.valueOf(secret)+"\n";
			}
			
		} catch (EOFException ex){
			System.out.println(resultat);
		} catch (FileNotFoundException ex) {
			System.err.println(ex);
		} catch (IOException ex) {
			System.err.println(ex);
		}
	}

}
