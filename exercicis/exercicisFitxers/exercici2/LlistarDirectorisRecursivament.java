package exerciciFitxers;

import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class LlistarDirectorisRecursivament {

	public static void main(String[] args) {
		boolean sortir = false;

		if (args.length == 1) {
			Path dir = Paths.get(args[0]);
			List<Path> llista = new ArrayList<Path>();
			do {
				System.out.println("Fitxers del directori:" + dir);
				if (Files.isDirectory(dir)) {
					try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
						for (Path fitxer : stream) {
							String s = "";
							if (Files.isDirectory(fitxer)) {
								s += "d";
								llista.add(fitxer);
							} else {
								s += "-";
							}
							if (Files.isReadable(fitxer)) {
								s += "r";
							} else {
								s += "-";
							}
							if (Files.isWritable(fitxer)) {
								s += "w";
							} else {
								s += "-";
							}
							if (Files.isExecutable(fitxer)) {
								s += "x";
							} else {
								s += "-";
							}
							System.out.println(s + " " + fitxer.getFileName());
						}
					} catch (IOException | DirectoryIteratorException ex) {
						System.err.println(ex);
					}
				} 
				else {
					System.err.println("Utilització: java LlistarDirectoris <directori>");
				}
				if (!llista.isEmpty())
					dir = llista.remove(llista.size() - 1);
				else
					sortir = true;
			} while (!sortir);

		}

	}

}
