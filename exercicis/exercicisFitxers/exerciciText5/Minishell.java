package exerciciText5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Minishell {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String opcio = "", dir = System.getProperty("user.dir");
		Minishell minishell = new Minishell();
		while (!opcio.equals("surt")) {
			System.out.print("Escull una opci� (cd, ls, cat): \n" + dir + " > ");
			opcio = sc.nextLine();
			String[] ordre = opcio.split(" ");
			if (ordre[0].equals("ls")) {
				minishell.ls(dir);
			} else if (ordre[0].equals("cd")) {
				dir = minishell.cd(dir, ordre[1]);
			} else if (ordre[0].equals("cat")) {
				if (ordre.length > 1) {
					minishell.cat(ordre[1]);
				}
				else{
					System.err.println("�s de cat: cat <fitxer>");
				}
			}
		}
		sc.close();
	}

	public void ls(String dir) {
		System.out.println("Fitxers del directori:" + dir);
		if (Files.isDirectory(Paths.get(dir))) {
			try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(dir))) {
				for (Path fitxer : stream) {
					String s = "";
					if (Files.isDirectory(fitxer)) {
						s += "d";
					} else {
						s += "-";
					}
					if (Files.isReadable(fitxer)) {
						s += "r";
					} else {
						s += "-";
					}
					if (Files.isWritable(fitxer)) {
						s += "w";
					} else {
						s += "-";
					}
					if (Files.isExecutable(fitxer)) {
						s += "x";
					} else {
						s += "-";
					}
					System.out.println(s + " " + fitxer.getFileName());
				}
			} catch (IOException | DirectoryIteratorException ex) {
				System.err.println(ex);
			}
		} else {
			System.err.println("Utilitzaci�: java LlistarDirectoris <directori>");
		}
	}

	public String cd(String dir, String dir2) {
		if (Files.isDirectory(Paths.get(dir2))) {
			return dir2;
		} else {
			System.err.println("Directori Incorrecte.");
			return dir;
		}
	}

	public void cat(String fitxer) {
		String s;
		if (Files.isRegularFile(Paths.get(fitxer))) {
			try (BufferedReader lector = new BufferedReader(new FileReader(fitxer))) {
				System.out.println("\nInici del fitxer " + fitxer + ":\n");
				while ((s = lector.readLine()) != null)
					System.out.println(s);
				System.out.println("\nFinal del fitxer " + fitxer + ".\n");
			} catch (IOException e) {
				System.err.println(e);
			}
		} else {
			System.err.println("ERROR. No has escollit un fitxer.");
		}
	}
}
