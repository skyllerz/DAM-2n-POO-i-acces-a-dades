package exerciciAccesAleatori2;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class SobreescriurePais {

	private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
		StringBuilder b = new StringBuilder();
		char ch = ' ';
		for (int i = 0; i < nChars; i++) {
			ch = fitxer.readChar();
			if (ch != '\0')
				b.append(ch);
		}
		return b.toString();
	}

	public static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		int id, poblacio;
		String nom, codi, capital;
		System.out.println("Introdueix el camp a sobreescriure: ");
		id = scanner.nextInt();
		scanner.nextLine();
		System.out.println();
		mostrarPais(id);
		System.out.println("Introdueix nom: ");
		nom = scanner.next();
		scanner.nextLine();
		System.out.println("Introdueix codi: ");
		codi = scanner.next();
		scanner.nextLine();
		System.out.println("Introdueix capital: ");
		capital = scanner.next();
		scanner.nextLine();
		System.out.println("Introdueix poblacio: ");
		poblacio = scanner.nextInt();
		scanner.nextLine();
		sobreescriuPais(id, nom, codi, capital, poblacio);
		mostrarPais(id);
	}

	public static void mostrarPais(int id) {
		Pais p;
		String nom, capital, codiISO;
		int poblacio;
		long pos = 0;
		try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "r")) {
			pos = (id - 1) * 174;
			if (pos < 0 || pos >= fitxer.length())
				throw new IOException("N�mero de registre inv�lid.");
			fitxer.seek(pos);
			System.out.println("Pa�s: " + fitxer.readInt());
			nom = readChars(fitxer, 40);
			codiISO = readChars(fitxer, 3);
			capital = readChars(fitxer, 40);
			poblacio = fitxer.readInt();
			p = new Pais(nom, codiISO, capital);
			p.setPoblacio(poblacio);
			System.out.println(p);

		} catch (IOException e) {
			System.err.println(e);
		}
	}

	public static void sobreescriuPais(int id, String nom, String codi, String capital, int poblacio) {
		long pos = 0;
		StringBuilder b = null;
		try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "rw")) {
			pos = (id - 1) * 174;
			if (pos < 0 || pos >= fitxer.length())
				throw new IOException("N�mero de registre inv�lid.");
			if (nom.length() > 0 && !nom.equals("")) {
				fitxer.seek(pos + 4);
				b = new StringBuilder(nom);
				b.setLength(40);
				fitxer.writeChars(b.toString());
			} else {
				throw new IOException("Nom inv�lid.");
			}
			if (codi.length() > 0 && codi.length() <= 3 && !codi.equals("")) {
				fitxer.seek(pos + 84);
				fitxer.writeChars(codi);
			} else {
				throw new IOException("Codi inv�lid");
			}
			if (capital.length() > 0 && !capital.equals("")) {
				fitxer.seek(pos + 90);
				b = new StringBuilder(capital);
				b.setLength(40);
				fitxer.writeChars(b.toString());
			} else {
				throw new IOException("Capital inv�lida");
			}
			if (poblacio > 0) {
				fitxer.seek(pos + 170);
				fitxer.writeInt(poblacio);
			} else {
				throw new IOException("La poblaci� ha de ser positiva");
			}
		} catch (IOException e) {
			System.err.println(e);
		}
	}

}
