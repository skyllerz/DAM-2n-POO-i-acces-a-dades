package exerciciXML2;

import java.io.IOException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class LlegirXMLAmbSAX {
	public static void main(String[] args) {
		if (args.length == 1) {
			try {
				XMLReader processadorXML = XMLReaderFactory.createXMLReader();
				processadorXML.setContentHandler(new ControladorSAX());
				processadorXML.parse(new InputSource(args[0]));
			} catch (SAXException | IOException e) {
				System.err.println(e.getMessage());
			}
		}
	}
}