package exerciciText2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class mostrarLiniesParaula {

	public static void main(String[] args) {
		String s, nomFitxer, paraula;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix la paraula fitxer: ");
		paraula = sc.next();
		sc.nextLine();
		System.out.println("Introdueix nom de fitxer: ");
		nomFitxer = sc.next();
		sc.nextLine();

		try (BufferedReader lector = new BufferedReader(new FileReader(nomFitxer))) {
			while ((s = lector.readLine()) != null)
				if (s.contains(paraula)) {
					System.out.println(s);
				}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		sc.close();
	}

}
