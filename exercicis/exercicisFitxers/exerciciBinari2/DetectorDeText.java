
package exerciciBinari2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class DetectorDeText {

	public static void main(String[] args) {
		int b;
		char c;
		String s;
		for (int i = 0; i < args.length; i++) {
			s = " ";
			b = 0;
			try (FileInputStream lector = new FileInputStream(args[i])) {
				while (b != -1) {
					b = lector.read();
					c = (char) b;
					if (Character.isLetter(c)) {
						s += c;
					}
				}
			} catch (FileNotFoundException e) {
				System.err.println("Aquest fitxer no existeix.");
			} catch (IOException e) {
				System.err.println(e);
			}
			System.out.println("Fitxer:" + args[i]);
			System.out.println("Contingut: " + s);
			System.out.println(s.length());
			System.out.println();
		}
	}

}
