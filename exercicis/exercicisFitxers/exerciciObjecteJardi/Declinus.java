package exerciciObjecteJardi;

public class Declinus extends Planta{
	private static final long serialVersionUID = 1L;

	/* Utilitzo les variables creix y decreix per fer que un torn creixin/decreixin i un altre no, 
	mentres que la variable comencaADecreixer nom�s t� la funci� de indicar quan deixa de creixer i 
	poder llan�ar una llavor abans de comen�ar a decreixer. */
	
	boolean creix=true, comencaADecreixer=false,decreix=false; 
	@Override
	public Llavor creix(){
		Declinus declinus = new Declinus();
		Llavor llavor = new Llavor(declinus);
		if (creix && !comencaADecreixer){
			altura++;
			creix=false;
			esViva=true;
		}
		else if (!creix && !comencaADecreixer){
			creix=true;
		}
		if (altura==4 && !comencaADecreixer){
			comencaADecreixer=true;
			return llavor;
		}
		if (altura==4 && comencaADecreixer && !decreix){
			decreix=true;
			return llavor;
		}
		if (decreix && comencaADecreixer){
			altura--;
			decreix=false;
		}
		else if (!decreix && comencaADecreixer){
			decreix=true;
		}
		if(altura==0){
			esViva=false;
		}
		
		
		return null;
	}

	public char getChar(int nivell) {
		if (nivell==altura){
			return '*';
		}
		else if (nivell<altura){
			return ':';
		}
		else{
			return ' ';
		}
	}
}

