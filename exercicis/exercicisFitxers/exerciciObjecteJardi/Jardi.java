package exerciciObjecteJardi;

import java.io.Serializable;

public class Jardi implements Serializable {
	private static final long serialVersionUID = 1L;
	
	Planta jardi[];
	
	public Jardi(int mida){
		if (mida<=0){
			mida=10;
		}
		jardi = new Planta[mida];
	}
	public void temps(){
		Planta planta;
		int posicioLlavor;
		for(int i=0;i<jardi.length;i++){
			if (jardi[i]!=null){
				planta=jardi[i].creix(); //Si la posicio esta buida, es fa creix()
				if (planta instanceof Llavor){
					posicioLlavor= i - jardi[i].escampaLlavor(); //Si es llavor, fa escampaLlavor() i guarda la posicio.
					if (posicioLlavor>0 && posicioLlavor<jardi.length && jardi[posicioLlavor]==null){
						jardi[posicioLlavor]=planta; //Si la posicio on ha caigut la llavor esta buida, es col�locar� aquesta llavor.
						
					}
					
				}
				else if (planta instanceof Planta){
					jardi[i]=planta;
				}
				if (jardi[i].esViva()==false){
					jardi[i]=null;
				}
			}
		
		}
	}
	@Override
	public String toString(){
		String s = "";
		for(int i=12;i>=0;i--){
			for(int j=0;j<jardi.length;j++){
				if(jardi[j]!=null){
					s+=jardi[j].getChar(i);
				}
				else{
					s+=" ";
				}
			}
			s+="\n";
		}
		for(int j=0;j<jardi.length;j++){
			s+="_";
		}
		s+="\n";

		return s;		
	}
	
	boolean plantaLlavor(Planta novaPlanta, int pos){
		if(jardi[pos]==null){
			jardi[pos]=novaPlanta;
			return true;
		}
		else{
			return false;
		}
	
	}
	
}
