package exerciciObjecteJardi;

public class Llavor extends Planta{
	private static final long serialVersionUID = 1L;
	Planta planta;
	int temps=0;
	public Llavor(Planta planta){
		if (planta instanceof Llavor) {
			throw new IllegalArgumentException();
		}
		else{
			this.planta=planta;
		}
	}
	public Planta creix(){
		if (temps==5){
			return planta;
		}
		else{
			temps++;
			return null;
		}
	}
	public char getChar(int nivell) {
		if (nivell!=0){
			return ' '; 
		}
		else{
			return '.';	
		}
	}

}
