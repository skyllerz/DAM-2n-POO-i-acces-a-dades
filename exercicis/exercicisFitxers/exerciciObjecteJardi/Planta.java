package exerciciObjecteJardi;

import java.io.Serializable;
import java.util.Random;

public abstract class Planta implements Serializable {
	private static final long serialVersionUID = 1L;
	
	protected boolean esViva=true;
	protected int altura;

	public Planta creix(){
		if(altura<10){
			altura++;
		}
		else{
			esViva=false;
		}
		return null;
		
	}
	public int escampaLlavor(){
		int posicio;
		Random  rnd = new Random();
		do{
			posicio=rnd.nextInt(5)-2;
		}while(posicio==0);
		return posicio;
	}
	public int getAltura(){
		return altura;
	}
	public boolean esViva(){
		return esViva;
	}
	public abstract char getChar(int nivell);
}


