package exerciciObjecteJardi;

public class Altibus extends Planta{
	private static final long serialVersionUID = 1L;

	public Llavor creix(){
		super.creix();
		Altibus altibus = new Altibus();
		Llavor llavor = new Llavor(altibus);
		if (altura>7){
			return llavor;
		}
		return null;
	}

	public char getChar(int nivell) {
		
		if (nivell==altura){
			return 'O';
		}
		else if (nivell<altura){
			return '|';
		}
		else{
			return ' ';
		}
		
	}
}
