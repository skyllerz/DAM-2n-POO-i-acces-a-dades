package exerciciObjecteJardi;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		String sortir = new String();
		Scanner sc = new Scanner(System.in);
		Jardi jardi = new Jardi(80);
		Planta declinus = new Declinus();
		Planta declinus2 = new Declinus();
		Planta declinus3 = new Declinus();
		Planta altibus = new Altibus();
		Planta altibus2 = new Altibus();
		Planta altibus3 = new Altibus();
		jardi.plantaLlavor(altibus, 10);
		jardi.plantaLlavor(altibus2, 20);
		jardi.plantaLlavor(altibus3, 30);
		jardi.plantaLlavor(declinus, 40);
		jardi.plantaLlavor(declinus2, 50);
		jardi.plantaLlavor(declinus3, 60);
		sortir = sc.nextLine();

		try (ObjectInputStream lector = new ObjectInputStream(new FileInputStream("jardi.sav"));) {
			Jardi jardiGuardat = (Jardi) lector.readObject();
			if (jardiGuardat instanceof Jardi) {
				jardi = jardiGuardat;
				System.out.println("Carregant jard� guardat...");
			}
		} catch (FileNotFoundException ex) {
			System.out.println("No hi ha un jard� guardat, comen�ant un de nou...");
		} catch (IOException ex) {
			System.err.println(ex);
		} catch (ClassNotFoundException ex) {
			System.err.println(ex);
		}
		do {
			jardi.temps();
			System.out.print(jardi.toString());
			sortir = sc.nextLine();
		} while (!sortir.equals("sortir"));

		try (ObjectOutputStream escriptor = new ObjectOutputStream(new FileOutputStream("jardi.sav"));) {
			escriptor.writeObject(jardi);
		} catch (IOException ex) {
			System.err.println(ex);
		}
		sc.close();
	}

}
