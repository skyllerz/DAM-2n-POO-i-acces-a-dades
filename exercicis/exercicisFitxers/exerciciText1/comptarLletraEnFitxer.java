package exerciciText1;

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class comptarLletraEnFitxer {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		System.out.println("Introdueix nom de fitxer: ");
		String nomFitxer = sc.next();
		sc.nextLine();
		int lectura, contador = 0;
		char ch;
		try (FileReader lector = new FileReader(nomFitxer)) {
			while ((lectura = lector.read()) != -1) {
				ch = (char) lectura;
				if (ch == 'a') {
					contador++;
				}
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		System.out.println("En el fitxer "+nomFitxer+" hi han "+contador+" 'a'.");
		sc.close();
	}

}
