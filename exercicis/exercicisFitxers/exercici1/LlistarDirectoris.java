package exerciciFitxers;

import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class LlistarDirectoris {

	public static void main(String[] args) {

		if(args.length==1){
			Path dir = Paths.get(args[0]);
			List<Path> llista = new ArrayList<Path>();
			System.out.println("Fitxers del directori:"+dir);
			if(Files.isDirectory(dir)){
				try(DirectoryStream<Path> stream = Files.newDirectoryStream(dir)){
					for (Path fitxer: stream) {
						String s = "";
						if (Files.isDirectory(fitxer)){
							s+="d";
							llista.add(fitxer);
						}
						else{
							s+="-";
						}
						if (Files.isReadable(fitxer)){
							s+="r";
						}
						else{
							s+="-";
						}
						if (Files.isWritable(fitxer)){
							s+="w";
						}
						else{
							s+="-";
						}
						if (Files.isExecutable(fitxer)){
							s+="x";						
						}
						else{
							s+="-";
						}
						System.out.println(s+" "+fitxer.getFileName());
					}
				}catch(IOException|DirectoryIteratorException ex){
					System.err.println(ex);
				}
			}
		
			else{
                System.err.println(dir.toString()+" no �s un directori");
			}
			
		}
		else {
			System.err.println("Utilitzaci�: java LlistarDirectoris <directori>");
		}
	}

}

