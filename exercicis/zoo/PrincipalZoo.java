package zoo;

import java.util.Scanner;

public class PrincipalZoo {

	public static void main(String[] args) {

		Zoo zoo = new Zoo();
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		String comandes = "";
		System.out.println("El zoo obre les seves portes");
		System.out.println("Comandes disponibles:");
		System.out.println("afegeix vaca / afegeix cocodril /");
		System.out.println("suprimeix vaca / suprimeix cocodril /");
		System.out.println("suprimeix tots vaca / suprimeix tots cocodril /");
		System.out.println("mira");
		while (!comandes.equals("surt")) {
			System.out.print("Introdueix comanda: ");
			comandes = sc.nextLine();
			if (comandes.equals("afegeix vaca")) {
				zoo.afegeixAnimal("vaca");
				System.out.println("Ha arribat al zoo una enorme vaca");
			} else if (comandes.equals("afegeix cocodril")) {
				zoo.afegeixAnimal("cocodril");
				System.out.println("Ha arribat al zoo un perill�s cocodril");
			} else if (comandes.equals("suprimeix vaca")) {
				zoo.suprimeixAnimal("vaca");
				System.out.println("S'ha traslladat una vaca a un altre zoo");
			} else if (comandes.equals("suprimeix cocodril")) {
				zoo.suprimeixAnimal("cocodril");
				System.out.println("S'ha traslladat un cocodril a un altre zoo");
			} else if (comandes.equals("suprimeix tots vaca")) {
				zoo.suprimeixTots("vaca");
				System.out.println("El zoo deixa de tenir vaques");
			} else if (comandes.equals("suprimeix tots cocodril")) {
				zoo.suprimeixTots("cocodril");
				System.out.println("El zoo deixa de tenir cocodrils");
			} else if (comandes.equals("mira")) {
				zoo.mira();
			}
		}
		System.out.println("El zoo tanca les seves portes");
	}

}
