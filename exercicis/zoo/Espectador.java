package zoo;

public class Espectador implements Esser {

	@Override
	public String accio(Zoo zoo) {
		Animal animal = zoo.mostraAnimal();
		if (animal instanceof Vaca) {
			return "Un espectador mira una vaca";
		} else if (animal instanceof Cocodril) {
			return "Un espectador mira a un perill�s cocodril";
		} else {
			return "Un espectador no sap a on mirar perqu� no troba animals";
		}
	}

}
