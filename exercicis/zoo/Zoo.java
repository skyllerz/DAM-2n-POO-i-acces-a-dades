package zoo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Zoo {
	private Random rnd = new Random();

	List<Animal> llista = new ArrayList<Animal>();
	Espectador espectador = new Espectador();

	public Animal mostraAnimal() {
		Animal animalAleatori;
		if (llista.isEmpty())
			return null;
		else {
			animalAleatori = llista.get(rnd.nextInt(llista.size()));
			return animalAleatori;
		}
	}

	public void afegeixAnimal(String cadena) {
		if (cadena.equals("vaca")) {
			Animal vaca = new Vaca();
			llista.add(vaca);
		}
		if (cadena.equals("cocodril")) {
			Animal cocodril = new Cocodril();
			llista.add(cocodril);
		}
	}

	public void suprimeixAnimal(String cadena) {
		Iterator<Animal> it = llista.iterator();
		boolean animalEsborrat = false;
		if (cadena.equals("vaca")) {
			Animal vaca;
			while (it.hasNext() && !animalEsborrat) {
				vaca = it.next();
				if (vaca instanceof Vaca) {
					it.remove();
					animalEsborrat = true;
				}
			}
		} else if (cadena.equals("cocodril")) {
			Animal cocodril;
			while (it.hasNext() && !animalEsborrat) {
				cocodril = it.next();
				if (cocodril instanceof Cocodril) {
					it.remove();
					animalEsborrat = true;
				}
			}
		}

	}

	public void suprimeixAnimal(Animal animal) {
		llista.remove(animal);
	}

	public void suprimeixTots(String cadena) {
		Iterator<Animal> it = llista.iterator();
		if (cadena.equals("vaca")) {
			Animal vaca;
			while (it.hasNext()) {
				vaca = it.next();
				if (vaca instanceof Vaca) {
					it.remove();
				}
			}
		} else if (cadena.equals("cocodril")) {
			Animal cocodril;
			while (it.hasNext()) {
				cocodril = it.next();
				if (cocodril instanceof Cocodril) {
					it.remove();
				}
			}
		}
	}

	public void mira() {
		int random = rnd.nextInt(2);
		if (random == 0) {
			System.out.println(espectador.accio(this));
		} else if (random == 1) {
			if (llista.isEmpty()) {
				System.out.println("Quin zoo m�s avorrit! No t� cap animal");
			} else {

				Animal animal = mostraAnimal();
				System.out.println(animal.accio(this));

			}
		}

	}
}
