package zoo;

public class Cocodril extends Animal {

	@Override
	public String mou(Zoo zoo) {
		return "Un cocodril neda estany amunt estany avall";
	}

	@Override
	public String alimenta(Zoo zoo) {
		Animal animal = zoo.mostraAnimal();
		if (animal == this) {
			return "Un cocodril busca a qui es pot menjar";
		} else {
			zoo.suprimeixAnimal(animal);
			if (animal instanceof Vaca) {
				return "Un cocodril es menja una vaca!";
			} else if (animal instanceof Cocodril) {
				return "Un cocodril es menja un altre cocodril";
			}
		}
		return "Un cocodril busca a qui es pot menjar";
	}

	@Override
	public String expressa(Zoo zoo) {
		return "Un cocodril obre la boca plena de dents";
	}

}
