package zoo;

import java.util.Random;

public abstract class Animal implements Esser {
	private Random rnd = new Random();
	int cridaMetode;

	public abstract String mou(Zoo zoo);

	public abstract String alimenta(Zoo zoo);

	public abstract String expressa(Zoo zoo);

	public String accio(Zoo zoo) {
		cridaMetode = rnd.nextInt(3);
		if (cridaMetode == 0) {
			return mou(zoo);
		} else if (cridaMetode == 1) {
			return alimenta(zoo);
		} else {
			return expressa(zoo);
		}
	};
}
