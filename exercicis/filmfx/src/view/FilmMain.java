package view;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FilmMain extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		stage.setScene(new Scene(new FilmUI(), 800, 350));
		stage.show();
	}

}
