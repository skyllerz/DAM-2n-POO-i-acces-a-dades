package view;

import controller.FilmController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import model.Film;

public class FilmUI extends BorderPane {
	private Label msgLabel = new Label();
	private TextField idField = new TextField();
	private TextField titleField = new TextField();
	private TextArea descField = new TextArea();
	private TextField releaseYearField = new TextField();
	private TextField languageField = new TextField();
	private TextField rDurationField = new TextField();
	private TextField rRateField = new TextField();
	private TextField replacementCostField = new TextField();
	private TextField lengthField = new TextField();

	private Button createButton = new Button("New...");
	private Button updateButton = new Button("Update");
	private Button deleteButton = new Button("Delete");
	private Button firstButton = new Button("<<");
	private Button prevButton = new Button("<");
	private Button nextButton = new Button(">");
	private Button lastButton = new Button(">>");
	private ObservableList<String> options = FXCollections.observableArrayList("G", "PG", "PG-13", "R", "NC-17");
	private ComboBox<String> ratings = new ComboBox<String>(options);
	private FilmController bean = new FilmController();

	public FilmUI() {
		setPadding(new Insets(10, 10, 10, 10));
		setTop(msgLabel);
		setCenter(initFields());
		setBottom(initButtons());
		setFieldData(bean.moveFirst());
	}

	private Pane initButtons() {
		HBox box = new HBox();
		ButtonHandler handler = new ButtonHandler();
		box.setAlignment(Pos.CENTER);
		box.setSpacing(5);
		box.getChildren().add(createButton);
		createButton.setOnAction(handler);
		box.getChildren().add(updateButton);
		updateButton.setOnAction(handler);
		box.getChildren().add(deleteButton);
		deleteButton.setOnAction(handler);
		box.getChildren().add(firstButton);
		firstButton.setOnAction(handler);
		box.getChildren().add(prevButton);
		prevButton.setOnAction(handler);
		box.getChildren().add(nextButton);
		nextButton.setOnAction(handler);
		box.getChildren().add(lastButton);
		lastButton.setOnAction(handler);
		return box;
	}

	private Pane initFields() {
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setHgap(20);
		grid.setVgap(2);
		grid.add(new Label("ID"), 0, 0);
		grid.add(idField, 1, 0);
		idField.setEditable(false);
		grid.add(new Label("Title"), 2, 0);
		grid.add(titleField, 3, 0);
		grid.add(new Label("Description"), 0, 1);
		descField.setWrapText(true);
		descField.setMinHeight(50);
		descField.setMaxHeight(100);
		grid.add(descField, 1, 1, 3, 1);
		grid.add(new Label("Release year"), 0, 2);
		grid.add(releaseYearField, 1, 2);
		grid.add(new Label("Language"), 2, 2);
		grid.add(languageField, 3, 2);
		grid.add(new Label("Rental duration"), 0, 3);
		grid.add(rDurationField, 1, 3);
		grid.add(new Label("Rental rate"), 2, 3);
		grid.add(rRateField, 3, 3);
		grid.add(new Label("Replacement cost"), 0, 4);
		grid.add(new Label("Ratings"), 0, 5);
		grid.add(ratings, 1, 5);
		grid.add(replacementCostField, 1, 4);
		grid.add(new Label("Length"), 2, 4);
		grid.add(lengthField, 3, 4);
		return grid;
	}

	private Film getFieldData() {
		Film f = new Film();
		f.setFilmId(Integer.parseInt(idField.getText()));
		f.setTitle(titleField.getText());
		f.setDescription(descField.getText());
		if (!releaseYearField.getText().equals(""))
			f.setReleaseYear(Integer.parseInt(releaseYearField.getText()));
		else
			f.setReleaseYear(0);
		f.setLanguageId(Integer.parseInt(languageField.getText()));
		f.setRentalDuration(Integer.parseInt(rDurationField.getText()));
		f.setRentalRate(Float.parseFloat(rRateField.getText()));
		f.setReplacementCost(Math.round(100 * Float.parseFloat(replacementCostField.getText())));
		f.setLength(Integer.parseInt(lengthField.getText()));
		f.setRating(ratings.getValue());
		return f;
	}

	private void setFieldData(Film f) {
		idField.setText("" + f.getFilmId());
		titleField.setText(f.getTitle());
		descField.setText(f.getDescription());
		releaseYearField.setText("" + f.getReleaseYear());
		languageField.setText("" + f.getLanguageId());
		rDurationField.setText("" + f.getRentalDuration());
		rRateField.setText("" + f.getRentalRate());
		replacementCostField.setText("" + Float.parseFloat("" + f.getReplacementCost()) / 100);
		System.out.println("Valor de ReplacementCost en l'objecte Film: " + f.getReplacementCost());
		lengthField.setText("" + f.getLength());
		ratings.setPromptText(f.getRating());

	}

	private boolean isEmptyReleaseYearField() {
		int any;
		if (!releaseYearField.getText().equals("")) {
			any = Integer.parseInt(releaseYearField.getText());
			if (any >= 1900 && any <= 2200) {
				return true;
			}
		} else
			return false;
		return false;
	}

	private boolean isEmptyFieldData() {
		return titleField.equals("");
	}

	private class ButtonHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent e) {
			Film f = getFieldData();
			if (e.getSource().equals(createButton) && createButton.getText().equals("Save")) {
				if (!isEmptyReleaseYearField()) {
					msgLabel.setText("The year must be bigger than 1900 or smaller than 2200");
					msgLabel.setTextFill(Color.RED);
					return;
				}
				if (bean.create(f) != null)
					msgLabel.setText("New film created successfully.");
				msgLabel.setTextFill(Color.BLACK);
				ratings.getSelectionModel().clearSelection();

				createButton.setText("New...");
			} else if (e.getSource().equals(createButton) && createButton.getText().equals("New...")) {
				setFieldData(new Film());
				createButton.setText("Save");
			} else if (e.getSource().equals(updateButton)) {
				if (!isEmptyReleaseYearField()) {
					msgLabel.setText("The year must be bigger than 1900 or smaller than 2200");
					msgLabel.setTextFill(Color.RED);
					return;
				}
				if (bean.update(f) != null)
					msgLabel.setText("Film with ID:" + String.valueOf(f.getFilmId() + " updated successfully"));
				msgLabel.setTextFill(Color.BLACK);
			} else if (e.getSource().equals(deleteButton)) {

				if (isEmptyFieldData()) {
					msgLabel.setText("Cannot delete an empty record");
					return;
				}
				f = bean.getCurrent();
				bean.delete();
				msgLabel.setText("Film with ID:" + String.valueOf(f.getFilmId() + " deleted successfully"));
			} else if (e.getSource().equals(firstButton)) {
				setFieldData(bean.moveFirst());
			} else if (e.getSource().equals(prevButton)) {
				setFieldData(bean.movePrevious());
			} else if (e.getSource().equals(nextButton)) {
				setFieldData(bean.moveNext());
			} else if (e.getSource().equals(lastButton)) {
				setFieldData(bean.moveLast());
			}
		}
	}
}
