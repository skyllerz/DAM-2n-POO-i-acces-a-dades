package insercioDocuments;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Exercici1 {

	private MongoClient client = new MongoClient();
	private MongoDatabase db = client.getDatabase("db");
	private MongoCollection<Document> coll = db.getCollection("zips");

	public static void main(String[] args) {
		long tempsInicial, tempsFinal;
		Exercici1 exercici1 = new Exercici1();
		exercici1.coll.drop();
		String[] paraulesZips = null;
		String[] loc = null;
		String s;
		int contador = 0;
		if (args.length == 1) {
			try (BufferedReader lector = new BufferedReader(new FileReader(args[0]))) {

				tempsInicial = System.currentTimeMillis();

				while ((s = lector.readLine()) != null) {

					s = s.replaceAll(",", "");
					s = s.replaceAll(":", "");
					s = s.replaceAll("\\[", "");
					s = s.replaceAll("\\]", "");
					paraulesZips = s.split("\"");

					paraulesZips[3] = paraulesZips[3].trim();
					paraulesZips[7] = paraulesZips[7].trim();
					paraulesZips[12] = paraulesZips[12].trim();
					paraulesZips[15] = paraulesZips[15].trim();

					loc = paraulesZips[10].split(" ");
					exercici1.insertarRegistres(paraulesZips, loc);

					contador++;
				}
				tempsFinal = System.currentTimeMillis();

				exercici1.client.close();

				System.out.println("\nRegistres Insertats: " + contador);
				System.out.println("Temps Trigat: " + (tempsFinal - tempsInicial) + "ms.");

			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}

	}

	public void insertarRegistres(String[] paraulesZips, String[] locs) {
		List<Float> loc = new ArrayList<Float>();
		loc.add(Float.parseFloat(locs[3]));
		loc.add(Float.parseFloat(locs[4]));
		Document document = new Document("_id", paraulesZips[3]).append("city", paraulesZips[7]).append("loc", loc)
				.append("pop", Integer.parseInt(paraulesZips[12])).append("state", paraulesZips[15]);
		System.out.println(document.toJson());
		coll.insertOne(document);
		System.out.println(document.toJson());
	}

}
