package consultesBasiques;

import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Sorts.orderBy;

import java.util.Set;
import java.util.TreeSet;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Exercici4 {

	public static void main(String[] args) {

		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("db");
		MongoCollection<Document> collection = db.getCollection("zips");
		Bson sort = orderBy(ascending("city"));
		Set <String> ciutats = new TreeSet<String>();
		collection.find(lt("pop", 50)).sort(sort).forEach((Document doc) -> ciutats.add(doc.getString("city")));
		System.out.println("Poblacions amb menys de 50 habitants.");
		for (String string : ciutats) {
			System.out.println(string);

		}
		client.close();
	}

}
