package consultesBasiques;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Sorts.orderBy;

import java.util.Set;
import java.util.TreeSet;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Exercici5 {

	public static void main(String[] args) {

		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("db");
		MongoCollection<Document> collection = db.getCollection("zips");
		Bson sortState = orderBy(ascending("state"));
		Bson smallestPostalCode = orderBy(ascending("_id"));
		Bson biggestPostalCode = orderBy(descending("_id"));
		Set<String> states = new TreeSet<String>();
		collection.find().sort(sortState).forEach((Document doc) -> states.add(doc.getString("state")));
		System.out.println("Poblacions amb menys de 50 habitants.");
		for (String state : states) {
			System.out.println("Estat: " + state);
			collection.find(eq("state", state)).sort(smallestPostalCode).limit(1)
					.forEach((Document doc) -> System.out.println("Codi postal m�s petit: "+doc.getString("_id")+"\nPoblaci� del codi postal: "+doc.getString("city")));
			collection.find(eq("state", state)).sort(biggestPostalCode).limit(1)
					.forEach((Document doc) -> System.out.println("Codi postal m�s gran: "+doc.getString("_id")+"\nPoblaci� del codi postal: "+doc.getString("city")));
			
			System.out.println();
		}
		client.close();
	}

}
