package consultesBasiques;

import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Sorts.orderBy;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Exercici3 {

	public static void main(String[] args) {

		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("db");
		MongoCollection<Document> collection = db.getCollection("zips");
		Bson projection = fields(include("city", "loc"), excludeId());
		Bson sort = orderBy(ascending("loc.1"));
		Document codiPostal = collection.find().skip((int) collection.count() / 2).sort(sort).projection(projection)
				.first();
		System.out.println(codiPostal.toJson());
		client.close();
	}

}
