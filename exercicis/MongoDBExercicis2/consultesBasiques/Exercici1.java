package consultesBasiques;

import static com.mongodb.client.model.Filters.eq;

import java.util.Scanner;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Exercici1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("db");
		MongoCollection<Document> collection = db.getCollection("zips");
		System.out.println();
		System.out.print("Introdueix una poblaci�: ");
		String poblacio = sc.nextLine();
		System.out.println("\nCodis postals de " + poblacio + ": ");
		collection.find(eq("city", poblacio)).forEach((Document doc) -> System.out.println(doc.getString("_id")));
		System.out.println();
		client.close();
		sc.close();
	}

}
