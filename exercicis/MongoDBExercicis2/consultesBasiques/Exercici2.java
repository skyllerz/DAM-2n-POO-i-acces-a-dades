package consultesBasiques;

import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Sorts.orderBy;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Exercici2 {

	public static void main(String[] args) {

		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("db");
		MongoCollection<Document> collection = db.getCollection("zips");
		Bson sort = orderBy(descending("pop"));
		System.out.println("10 codis postals amb m�s poblaci�:");
		collection.find().sort(sort).limit(10).forEach((Document doc) -> System.out.println(doc.getString("_id")));
		client.close();
	}

}
