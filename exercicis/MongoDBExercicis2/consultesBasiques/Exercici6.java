package consultesBasiques;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Sorts.orderBy;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class Exercici6 {

	public static void main(String[] args) {
		int pop = 0;
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("db");
		MongoCollection<Document> collection = db.getCollection("zips");
		Bson sortId = orderBy(ascending("_id"));
		Bson filter = eq("city", "KANSAS CITY");
		try (MongoCursor<Document> cursor = collection.find(filter).sort(sortId).iterator()) {
			System.out.println("\nCodis postals de Kansas City:\n");
			while (cursor.hasNext()) {
				Document doc = cursor.next();
				System.out.println("Codi postal: " + doc.getString("_id") + "\nPoblaci�: " + doc.getInteger("pop"));
				pop += doc.getInteger("pop");
				System.out.println();
			}
			System.out.println("Poblaci� total en Kansas City: " + pop + "\n");
		}
		client.close();
	}

}
