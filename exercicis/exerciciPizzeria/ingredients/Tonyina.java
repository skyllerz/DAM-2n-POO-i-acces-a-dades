package ingredients;

import base.Ingredient;
import base.IngredientExtra;

public class Tonyina extends IngredientExtra implements Cloneable {

	public Tonyina(Ingredient ingredient) {
		super(ingredient);
	}

	@Override
	public String recepta() {
		String recepta = super.recepta() + "\n";
		return recepta + "El cuiner reparteix la tonyina per tota la pizza.";
	}

	@Override
	public double getPreu() {
		double preu = super.getPreu();
		return preu + 2;
	}

	@Override
	public String descripcio() {
		String descripcio = super.descripcio() + ",";
		return descripcio + " tonyina";
	}

	@Override
	public Ingredient clone() {
		return (Ingredient) super.clone();
	}

}
