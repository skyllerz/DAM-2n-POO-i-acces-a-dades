package ingredients;

import base.Ingredient;
import base.IngredientExtra;

public class Tomaquet extends IngredientExtra implements Cloneable {

	public Tomaquet(Ingredient ingredient) {
		super(ingredient);
	}

	@Override
	public String recepta() {
		String recepta = super.recepta() + "\n";
		return recepta + "El cuiner utilitza una esp�tula per escampar tom�quet per sobre la base.";
	}

	@Override
	public double getPreu() {
		double preu = super.getPreu();
		return preu + 2;
	}

	@Override
	public String descripcio() {
		String descripcio = super.descripcio() + ",";
		return descripcio + " tom�quet";
	}

	@Override
	public Ingredient clone() {
		return (Ingredient) super.clone();
	}

}
