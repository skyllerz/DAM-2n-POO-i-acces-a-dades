package ingredients;

import base.Ingredient;
import base.IngredientExtra;

public class Formatge extends IngredientExtra implements Cloneable {

	public Formatge(Ingredient ingredient) {
		super(ingredient);
	}

	@Override
	public String recepta() {
		String recepta = super.recepta() + "\n";
		return recepta + "El cuiner escampa formatge ratllat per sobre.";
	}

	@Override
	public double getPreu() {
		double preu = super.getPreu();
		return preu + 2;
	}

	@Override
	public String descripcio() {
		String descripcio = super.descripcio() + ",";
		return descripcio + " formatge ratllat";
	}

	@Override
	public Ingredient clone() {
		return (Ingredient) super.clone();
	}

}
