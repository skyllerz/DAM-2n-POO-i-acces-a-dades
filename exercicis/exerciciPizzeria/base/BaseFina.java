package base;

public class BaseFina implements Ingredient, Cloneable {

	@Override
	public String recepta() {
		return "El cuiner est�n una fin�ssima base de pizza sobre el marbre.";
	}

	@Override
	public double getPreu() {
		return 6.25;
	}

	@Override
	public String descripcio() {
		return "una base de pizza fina";
	}

	@Override
	public Ingredient clone() {
		try {
			return (Ingredient) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}
}
