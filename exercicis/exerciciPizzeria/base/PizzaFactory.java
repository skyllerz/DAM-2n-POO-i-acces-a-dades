package base;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class PizzaFactory {
	Map<String, Ingredient> receptesPizzes = new HashMap<String, Ingredient>();
	Set<String> nomPizzes = receptesPizzes.keySet();

	public void addPizza(String nomPizza, Ingredient pizza) {
		receptesPizzes.put(nomPizza.toLowerCase(), pizza);
	}

	public Set<String> getNomsPizzes() {
		return Collections.unmodifiableSet(receptesPizzes.keySet());
	}

	public Ingredient getPizzaByName(String nomPizza) {
		try {
			return receptesPizzes.get(nomPizza.toLowerCase()).clone();

		} catch (Exception e) {
			System.err.println("No hi ha cap pizza amb aquest nom");
		}
		return null;
	}
}
