package base;

public abstract class IngredientExtra implements Ingredient, Cloneable {
	private final Ingredient ingredient;

	public IngredientExtra(Ingredient ingredient) {
		this.ingredient = ingredient;
	}

	@Override
	public String recepta() {
		return ingredient.recepta();
	}

	@Override
	public double getPreu() {
		return ingredient.getPreu();
	}

	@Override
	public String descripcio() {
		return ingredient.descripcio();
	}

	@Override
	public Ingredient clone() {
		try {
			return (IngredientExtra) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}

}
