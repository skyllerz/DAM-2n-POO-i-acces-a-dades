package base;

import java.util.Scanner;

import ingredients.Formatge;
import ingredients.Tomaquet;
import ingredients.Tonyina;

public class Restaurant {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String comanda = "";

		PizzaFactory pizzeria = new PizzaFactory();
		pizzeria.addPizza("Margarita", new Formatge(new Tomaquet(new BaseFina())));
		pizzeria.addPizza("Marinera", new Formatge(new Tonyina(new BaseFina())));
		Ingredient pizza;
		System.out.println("Benvingut a la pizzeria Luigi!");
		while (!comanda.equalsIgnoreCase("n")) {
			System.out.println("\nAquestes s�n les nostres pizzes: ");
			for (String nom : pizzeria.getNomsPizzes())
				System.out.println(nom + " ");
			System.out.println("\nQuina pizza voleu? ");
			comanda = scanner.nextLine();
			try {
				pizza = pizzeria.getPizzaByName(comanda);
				System.out.println("El preu de la comanda �s de " + pizza.getPreu());
				System.out.println("Els ingredients de la pizza s�n: " + pizza.descripcio());
				System.out.println("Voleu continuar (s per acceptar)?");
				comanda = scanner.nextLine();
				if (comanda.equalsIgnoreCase("s")) {
					System.out.println(pizza.recepta());
					System.out.println("Aqu� t� la seva pizza!");
				}
			} catch (IllegalArgumentException e) {
				System.out.println(e.getMessage());
			}
			System.out.println("\nVoleu fer una altra comanda (n per sortir)?");
			comanda = scanner.nextLine();
		}
		System.out.println("Fins la propera!");
		scanner.close();
	}

}
