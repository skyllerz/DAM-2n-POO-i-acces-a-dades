package exercici1ComparableComparator;


public class Persona implements Comparable<Persona>{
	
	private int pes;
	private int edat;
	private int alcada;
	
	public Persona(int pes, int edat, int alcada) {
		super();
		this.pes = pes;
		this.edat = edat;
		this.alcada = alcada;
	}

	public int getPes() {
		return pes;
	}

	public void setPes(int pes) {
		this.pes = pes;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public int getAlcada() {
		return alcada;
	}

	public void setAlcada(int alcada) {
		this.alcada = alcada;
	}

	@Override
	public int compareTo(Persona p) {
		return alcada-p.getAlcada();
	}

}
	