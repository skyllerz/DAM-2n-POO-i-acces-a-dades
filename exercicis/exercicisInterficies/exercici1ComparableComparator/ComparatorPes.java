package exercici1ComparableComparator;

import java.util.Comparator;

public class ComparatorPes implements Comparator <Persona>{

	@Override
	public int compare(Persona p1, Persona p2) {
		return p1.getPes()-p2.getPes();
	}

}
