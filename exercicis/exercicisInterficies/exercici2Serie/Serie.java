package exercici2Serie;

public interface Serie {

	public void init();
	public void init(double llavor);
	public double seguentNumero();
}
