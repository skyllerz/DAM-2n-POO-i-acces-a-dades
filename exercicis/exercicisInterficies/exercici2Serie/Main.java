package exercici2Serie;

public class Main {
	public static void mostra20Primers(Serie s, double llavor){
		s.init(llavor);
		for(int a=0;a<20;a++){
			System.out.print(s.seguentNumero()+" ");
		}
	}
	public static void main(String[] args) {
		Serie s = new PerDos();
		mostra20Primers(s, 2);
	}

}
