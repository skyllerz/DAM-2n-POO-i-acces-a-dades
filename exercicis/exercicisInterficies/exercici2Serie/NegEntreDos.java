package exercici2Serie;

public class NegEntreDos implements Serie{
	
	private double llavor;
		
	@Override
	public void init() {
		llavor=1;
	}
	
	@Override
	public void init(double llavor) {
		this.llavor=llavor;
	}

	@Override
	public double seguentNumero() {
		llavor=(llavor/2)*(-1);
		return llavor;
	}

}
