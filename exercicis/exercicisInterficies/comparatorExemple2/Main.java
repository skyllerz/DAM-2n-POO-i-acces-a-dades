/*1. Creeu la classe Persona amb els atributs pes, edat i al�ada. Implementeu
la interf�cie Comparable per fer que es compari l'al�ada. Creeu un
Comparator per permetre que dues persones es puguin comparar per l'edat, i un
altre per tal que es puguin comparar pel pes.*/

package comparatorExemple2;

import java.util.Arrays;

public class Main {
	

	public static void main(String[] args) {
		
		Persona p1 = new Persona (40,50,2);
		Persona p2 = new Persona (60,30,1);
		Persona [] p3 = new Persona [4];
		p3 [0]= new Persona (20,30,1);
		p3 [1]= new Persona (80,20,2);
		p3 [2]= new Persona (65,25,1);
		p3 [3]= new Persona (50,40,2);
		int r = p1.compareTo(p2);
		
		Persona.COMPARATOR_EDAT.compare(p1, p2);
		Persona.COMPARATOR_PES.compare(p1, p2);
		
		// Comparem Al�ada de p1 y p2.
		
		if (r>0){
			System.out.println();
			System.out.println("- p1 es m�s alt que p2.");
		}
		else if(r<0){
			System.out.println();
			System.out.println("- p1 es m�s baix que p2.");
		}
		else {
			System.out.println();
			System.out.println("- p1 s�n igual d'alts que p2.");
		}

		// Comparem Pes de p1 y p2.
		
		if (Persona.COMPARATOR_PES.compare(p1, p2)>0){
			System.out.println();
			System.out.println("- p1 pesa m�s que p2.");
		}
		else if(Persona.COMPARATOR_PES.compare(p1, p2)<0){
			System.out.println();
			System.out.println("- p1 pesa menys que p2.");
		}
		else {
			System.out.println();
			System.out.println("- p1 y p2 pesen igual.");
		}

		// Comparem Edat de p1 y p2.
		
		if (Persona.COMPARATOR_EDAT.compare(p1, p2)>0){
			System.out.println();
			System.out.println("- p1 es major que p2.");
		}
		else if(Persona.COMPARATOR_EDAT.compare(p1, p2)<0){
			System.out.println();
			System.out.println("- p1 es menor que p2.");
		}
		else {
			System.out.println();
			System.out.println("- p1 y p2 son de la mateixa edat.");
		}
		
		//Mostrem els valors dels comparators
		System.out.println();
		System.out.println("Valor de compareTo: "+r);
		System.out.println("Valor de ComparatorPes: "+Persona.COMPARATOR_PES.compare(p1, p2));
		System.out.println("Valor de ComparatorEdat: "+Persona.COMPARATOR_EDAT.compare(p1, p2));
		System.out.println();
		System.out.println("Abans de Arrays.sort");
		System.out.println();
		Persona.imprimir(p3);
		Arrays.sort(p3,Persona.COMPARATOR_EDAT);
		System.out.println();
		System.out.println("Despr�s de Arrays.sort");
		System.out.println();		
		Persona.imprimir(p3);
	}
}