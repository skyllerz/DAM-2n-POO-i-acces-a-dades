package comparatorExemple2;

import java.util.Comparator;

public class Persona implements Comparable<Persona>{
		private int pes;
		private int edat;
		private int alcada;
		
		public static final Comparator<Persona> COMPARATOR_EDAT = new Comparator<Persona>() {
			@Override
			public int compare(Persona p1, Persona p2) {
					return p1.getEdat()-p2.getEdat();
			}
		};
		
		public static final Comparator<Persona> COMPARATOR_PES = new Comparator<Persona>(){
			@Override
			public int compare(Persona p1, Persona p2) {
				
				return p1.getAlcada()-p2.getAlcada();
			}
			
		};
		
		public Persona(int pes, int edat, int alcada) {
			this.pes = pes;
			this.edat = edat;
			this.alcada = alcada;
		}

		public int getPes() {
			return pes;
		}

		public void setPes(int pes) {
			this.pes = pes;
		}

		public int getEdat() {
			return edat;
		}

		public void setEdat(int edat) {
			this.edat = edat;
		}

		public int getAlcada() {
			return alcada;
		}

		public void setAlcada(int alcada) {
			this.alcada = alcada;
		}

		@Override
		public int compareTo(Persona p) {
			return alcada-p.getAlcada();
		}
		
		public static void imprimir (Persona p3 []){
			for(int a=0;a<p3.length;a++){
				System.out.println("Peso: "+p3[a].getPes()+" Edad: "+p3[a].getEdat()+" Altura: "+p3[a].getAlcada());
			}
		}
		
}
