package exercici3JocDeRol;

import java.util.Arrays;
import java.util.Random;

public class Tirada implements Comparable<Tirada>,Cloneable{
	private int atac;
	private int defensa;
	private int [] tirada;
	private static Random  rnd = new Random();
	
	public Tirada(int atac,int defensa,int daus) {
		this.atac=atac;
		this.defensa=defensa;
		tirada = new int [daus];
		
		for(int a=0;a<tirada.length;a++){
			tirada[a]=rnd.nextInt(6)+1;
		}
		Arrays.sort(tirada);
	}
	@Override
	public String toString(){
		String s ="AT: "+atac+" DEF: "+defensa+" Daus ("+tirada.length+"): ";
		for(int a=0;a<tirada.length;a++){
			s+=tirada[a];
		}
		return s;		
	}
	public int getTiradaLength(){
		return tirada.length;
	}
	@Override
	public int compareTo(Tirada t1) {
		int contadorJugador1=0;
		int contadorJugador2=0;
		int mesGran,diferencia;
		int [] arrayGran;
		int [] arrayPetita;
		diferencia=t1.getTiradaLength()-tirada.length;
		if (diferencia>0){
			mesGran=t1.getTiradaLength();
			arrayPetita = new int [mesGran];
			arrayGran=t1.tirada;
			for(int a=0;a<t1.getTiradaLength();a++){
				if(a<diferencia){
					arrayPetita[a]=0;
				}
				else{
					arrayPetita[a]=tirada[a-diferencia];
				}
			}
		}
		else {
			mesGran=tirada.length;
			arrayPetita = new int [mesGran];
			arrayGran=tirada;
			for(int a=0;a<tirada.length;a++){
				if(a<diferencia*-1){
					arrayPetita[a]=0;
				}
				else{
					arrayPetita[a]=t1.tirada[a+diferencia];
				}
			}
		}
		for(int a=0;a<mesGran;a++){
			if(arrayPetita[a]+atac>arrayGran[a]+t1.defensa){
				contadorJugador1++;
			}
			else if (arrayGran[a]+t1.atac>arrayPetita[a]+defensa){
				contadorJugador2++;
			}
		}
				return contadorJugador1-contadorJugador2;
	}
}
