package exercici3JocDeRol;

public class Main {

	public static void main(String[] args) {
		
		Tirada jugador1 = new Tirada(3, 1, 4);
		Tirada jugador2 = new Tirada(2, 2, 5);
		
		System.out.println(jugador1.toString());
		System.out.println(jugador2.toString());
		System.out.println("Comparació de les dues tirades: "+jugador1.compareTo(jugador2));
	}

}
