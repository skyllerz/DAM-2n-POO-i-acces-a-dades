package comparatorExemple3;

import java.util.Comparator;

public class Persona implements Comparable<Persona>{
		private int pes;
		private int edat;
		private int alcada;
		
		public static final Comparator<Persona> COMPARATOR_EDAT = (p1,p2) -> p1.getEdat()-p2.getEdat();
		
		public static final Comparator<Persona> COMPARATOR_PES = (p1,p2) -> p1.getAlcada()-p2.getAlcada();
					
		public Persona(int pes, int edat, int alcada) {
			this.pes = pes;
			this.edat = edat;
			this.alcada = alcada;
		}

		public int getPes() {
			return pes;
		}

		public void setPes(int pes) {
			this.pes = pes;
		}

		public int getEdat() {
			return edat;
		}

		public void setEdat(int edat) {
			this.edat = edat;
		}

		public int getAlcada() {
			return alcada;
		}

		public void setAlcada(int alcada) {
			this.alcada = alcada;
		}

		@Override
		public int compareTo(Persona p) {
			return alcada-p.getAlcada();
		}
		
}
