package observerPattern;

import java.util.ArrayList;
import java.util.List;

public class ProdueixEsdeveniments {

	private List<Listener> listeners = new ArrayList<Listener>();
	
	public void addEventListener(Listener listener){
		listeners.add(listener);
	}
	
	public void creaEsdeveniment(int num){
		for (Listener listener : listeners) {
			listener.notifyEvent(num);
		}
	}
}
