package observerPattern;

public interface Listener {
	public void notifyEvent(int num);
}
