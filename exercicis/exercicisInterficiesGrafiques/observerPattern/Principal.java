package observerPattern;

public class Principal {

	public static void main(String[] args) {

		GuardaEsdeveniments guardaEsdeveniment = new GuardaEsdeveniments();
		MostraEsdeveniments mostraEsdeveniment = new MostraEsdeveniments();
		ProdueixEsdeveniments produeixEsdeveniment = new ProdueixEsdeveniments();
		
		produeixEsdeveniment.addEventListener(guardaEsdeveniment);
		produeixEsdeveniment.addEventListener(mostraEsdeveniment);
		
		produeixEsdeveniment.creaEsdeveniment(10);
		produeixEsdeveniment.creaEsdeveniment(5);
		produeixEsdeveniment.creaEsdeveniment(68);
		produeixEsdeveniment.creaEsdeveniment(23);
		System.out.println(guardaEsdeveniment.toString());
		
	}

}
