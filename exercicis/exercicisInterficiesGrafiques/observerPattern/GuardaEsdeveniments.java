package observerPattern;

import java.util.ArrayList;
import java.util.List;

public class GuardaEsdeveniments implements Listener{
	
	private List<Integer> events = new ArrayList<Integer>();
	
	@Override
	public void notifyEvent(int num) {
		events.add(num);
	}
	
	@Override
	public String toString(){
		String s="";
		for (Integer i : events) {
			s+= i + " ";
		}
		return s;
		
	}

}
