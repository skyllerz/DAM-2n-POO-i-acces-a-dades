package exercici5;

import java.util.Comparator;

public class Contacte implements Comparator<Contacte> {
	private String nom, cognom, adreca, telefon;

	public Contacte(String nom, String cognom, String adreca, String telefon) {
		this.nom = nom;
		this.cognom = cognom;
		this.adreca = adreca;
		this.telefon = telefon;
	}

	public Contacte() {

	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCognom() {
		return cognom;
	}

	public void setCognom(String cognom) {
		this.cognom = cognom;
	}

	public String getAdreca() {
		return adreca;
	}

	public void setAdreca(String adreca) {
		this.adreca = adreca;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	@Override
	public int compare(Contacte contacte1, Contacte contacte2) {
		if (contacte1.getCognom().equals(contacte2.getCognom()))
			return contacte1.getNom().compareTo(contacte2.getNom());
		else {
			return contacte2.getCognom().compareTo(contacte2.getCognom());
		}
	}

	@Override
	public String toString(){
		return "Nom: "+nom+"\nCognom: "+cognom+"\nAdre�a: "+adreca+"\nTel�fon: "+telefon+"\n";
	}
}
