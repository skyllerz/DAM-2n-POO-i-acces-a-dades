package exercici5;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Menu();
	}

	public static void Menu() {
		int opcio;
		String nom, nom2, cognom, cognom2, adreca, telefon;
		Agenda agenda = new Agenda();
		Contacte contacte;
		Scanner sc = new Scanner(System.in);
		do {
			System.out.println(" __________________________________");
			System.out.println("|                                  |");
			System.out.println("|              Menu                |");
			System.out.println("|__________________________________|");
			System.out.println("|                                  |");
			System.out.println("| 1- Afegir Contacte               |");
			System.out.println("| 2- Esborrar Contacte             |");
			System.out.println("| 3- Modificar dada d'un Contacte  |");
			System.out.println("| 4- Cerca d'un Contacte           |");
			System.out.println("| 5- Mostrar tots els contactes    |");
			System.out.println("| 0- Sortir                        |");
			System.out.println("|__________________________________|");
			System.out.println();
			System.out.println("Introdueix una opci�: ");
			opcio = sc.nextInt();
			switch (opcio) {
			case 0:
				break;
			case 1:
				System.out.println("Introdueix un nom: ");
				nom = sc.next();
				System.out.println("Introdueix un cognom: ");
				cognom = sc.next();
				System.out.println("Introdueix una adre�a: ");
				adreca = sc.next();
				System.out.println("Introdueix un tel�fon: ");
				telefon = sc.next();
				agenda.afegirElement(nom, cognom, adreca, telefon);
				break;
			case 2:
				System.out.println("Introdueix un nom: ");
				nom = sc.next();
				System.out.println("Introdueix un cognom: ");
				cognom = sc.next();
				try {
					agenda.esborrarElement(nom, cognom);
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 3:
				System.out.println(" __________________________________");
				System.out.println("|                                  |");
				System.out.println("| 3- Modificar dada d'un Contacte  |");
				System.out.println("|__________________________________|");
				System.out.println("|                                  |");
				System.out.println("| 1- Modificar Nom                 |");
				System.out.println("| 2- Modificar Cognom              |");
				System.out.println("| 3- Modificar Adre�a              |");
				System.out.println("| 4- Modificar Tel�fon             |");
				System.out.println("| 5- Enrere                        |");
				System.out.println("| 0- Sortir                        |");
				System.out.println("|__________________________________|");
				System.out.println();
				System.out.println("Introdueix una opci�: ");
				opcio = sc.nextInt();
				switch (opcio) {
				case 0:
					break;
				case 1:
					System.out.println("Introdueix el nom del contacte a modificar: ");
					nom = sc.next();
					System.out.println("Introdueix el cognom del contacte a modificar: ");
					cognom = sc.next();
					System.out.println("Introdueix el nou nom: ");
					nom2 = sc.next();
					try {
						agenda.modificarNom(nom, cognom, nom2);
					} catch (Exception e) {
						System.out.println();
						System.err.println(e.getMessage());
					}
					break;
				case 2:
					System.out.println("Introdueix el nom del contacte a modificar: ");
					nom = sc.next();
					System.out.println("Introdueix el cognom del contacte a modificar: ");
					cognom = sc.next();
					System.out.println("Introdueix el nou cognom: ");
					cognom2 = sc.next();
					try {
						agenda.modificarCognom(nom, cognom, cognom2);
					} catch (Exception e) {
						System.out.println();
						System.err.println(e.getMessage());
					}
					break;
				case 3:
					System.out.println("Introdueix el nom del contacte a modificar: ");
					nom = sc.next();
					System.out.println("Introdueix el cognom del contacte a modificar: ");
					cognom = sc.next();
					System.out.println("Introdueix la nova adre�a: ");
					adreca = sc.next();
					try {
						agenda.modificarAdreca(nom, cognom, adreca);
					} catch (Exception e) {
						System.out.println();
						System.err.println(e.getMessage());
					}
					break;
				case 4:
					System.out.println("Introdueix el nom del contacte a modificar: ");
					nom = sc.next();
					System.out.println("Introdueix el cognom del contacte a modificar: ");
					cognom = sc.next();
					System.out.println("Introdueix un nou tel�fon: ");
					telefon = sc.next();
					try {
						agenda.modificarTelefon(nom, cognom, telefon);
					} catch (Exception e) {
						System.out.println();
						System.err.println(e.getMessage());
					}
					break;
				case 5:
					break;
				default:
					System.out.println();
					System.err.println("Opci� err�nia");
					break;
				}
				break;
			case 4:
				System.out.println(" _____________________________________");
				System.out.println("|                                     |");
				System.out.println("|       4- Cerca d'un Contacte        |");
				System.out.println("|_____________________________________|");
				System.out.println("|                                     |");
				System.out.println("| 1- Buscar Contacte per Nom i Cognom |");
				System.out.println("| 2- Buscar Contacte per Adre�a       |");
				System.out.println("| 3- Buscar Contacte per Tel�fon      |");
				System.out.println("| 4- Enrere                           |");
				System.out.println("| 0- Sortir                           |");
				System.out.println("|_____________________________________|");
				System.out.println("");
				System.out.println("Introdueix una opci�: ");
				opcio = sc.nextInt();
				switch (opcio) {
				case 0:
					break;
				case 1:
					System.out.println("Introdueix un nom: ");
					nom = sc.next();
					System.out.println("Introdueix un cognom: ");
					cognom = sc.next();
					try {
						contacte = agenda.cercaContacte(nom, cognom);
						System.out.println(contacte);
					} catch (Exception e) {
						System.out.println();
						System.err.println(e.getMessage());
					}

					break;
				case 2:
					System.out.println("Introdueix una adre�a: ");
					adreca = sc.next();
					try {
						contacte = agenda.cercaContacteAdreca(adreca);
						System.out.println(contacte);
					} catch (Exception e) {
						System.out.println();
						System.err.println(e.getMessage());
					}

					break;
				case 3:
					System.out.println("Introdueix un tel�fon: ");
					telefon = sc.next();
					try {
						contacte = agenda.cercaContacteTelefon(telefon);
						System.out.println(contacte);
					} catch (Exception e) {
						System.err.println(e.getMessage());
					}
					break;
				case 4:
					break;
				default:
					System.out.println();
					System.err.println("Opci� err�nia");
					break;
				}
				break;
			case 5:
				System.out.println();
				if(agenda.toString().equals("L'agenda esta buida")){
					System.err.println(agenda);
				}
				else{
					System.out.println(agenda);
				}
				break;
			default:
				System.out.println();
				System.err.println("Opci� err�nia");
				break;
			}
		} while (opcio != 0);
		sc.close();
		System.out.println("Sortint de l'agenda...");
	}

}
