package exercici5;

import java.util.ArrayList;
import java.util.List;

public class Agenda {

	private List<Contacte> agenda = new ArrayList<Contacte>();

	public void afegirElement(String nom, String cognom, String adreca, String telefon) {
		Contacte contacte = new Contacte(nom, cognom, adreca, telefon);
		agenda.add(contacte);
		agenda.sort(contacte);
	}

	public int trobarPosicio(String nom, String cognom) throws Exception {
		int posicio = 0;
		boolean contacteNoTrobat = false;
		if (!agenda.isEmpty()) {
			for (int i = 0; i < agenda.size() && !contacteNoTrobat; i++) {
				if (agenda.get(i).getNom().equals(nom) && agenda.get(i).getCognom().equals(cognom)) {
					posicio = i;
					contacteNoTrobat = true;
					return posicio;
				}
				if (!contacteNoTrobat) {
					throw new Exception("No s'ha trobat o no existeix l'usuari");
				}
			}
		}
		else{
			throw new Exception("L'agenda esta buida");
		}
		return posicio;

	}

	public void esborrarElement(String nom, String cognom) throws Exception {
		agenda.remove(trobarPosicio(nom, cognom));
	}

	public void modificarNom(String nom, String cognom, String nomAModificar) throws Exception {
		agenda.get(trobarPosicio(nom, cognom)).setNom(nomAModificar);
	}

	public void modificarCognom(String nom, String cognom, String cognomAModificar) throws Exception {
		agenda.get(trobarPosicio(nom, cognom)).setCognom(cognomAModificar);
	}

	public void modificarAdreca(String nom, String cognom, String adrecaAModificar) throws Exception {
		agenda.get(trobarPosicio(nom, cognom)).setAdreca(adrecaAModificar);
	}

	public void modificarTelefon(String nom, String cognom, String telefonAModificar) throws Exception {
		agenda.get(trobarPosicio(nom, cognom)).setTelefon(telefonAModificar);
	}

	public Contacte cercaContacte(String nom, String cognom) throws Exception {
		Contacte contacte = new Contacte();
		contacte = agenda.get(trobarPosicio(nom, cognom));
		return contacte;
	}

	public Contacte cercaContacteAdreca(String adreca) throws Exception {
		Contacte contacte = new Contacte();
		boolean contacteNoTrobat = false;
		for (int i = 0; i < agenda.size() && !contacteNoTrobat; i++) {
			if (agenda.get(i).getAdreca().equals(adreca)) {
				contacte = agenda.get(i);
				contacteNoTrobat = true;
			}
		}
		if (!contacteNoTrobat) {
			throw new Exception("No s'ha trobat o no existeix l'usuari");
		}
		return contacte;
	}

	public Contacte cercaContacteTelefon(String telefon) throws Exception {
		Contacte contacte = new Contacte();
		boolean contacteNoTrobat = false;
		for (int i = 0; i < agenda.size() && !contacteNoTrobat; i++) {
			if (agenda.get(i).getTelefon().equals(telefon)) {
				contacte = agenda.get(i);
				contacteNoTrobat = true;
			}
		}
		if (!contacteNoTrobat) {
			throw new Exception("No s'ha trobat o no existeix l'usuari");
		}
		return contacte;
	}
	@Override
	public String toString(){
		String contacte="";
		if (agenda.isEmpty()){
			contacte="L'agenda esta buida";
		}
		else{
			for (int i = 0; i < agenda.size(); i++) {
				contacte+="Contacte "+(i+1)+"\n--------\n"+agenda.get(i).toString()+"\n";
			}
		}
		return contacte;
	}
}
