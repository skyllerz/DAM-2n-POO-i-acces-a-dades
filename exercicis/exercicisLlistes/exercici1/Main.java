package exercici1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		String cadena;
		List<String> llista = new ArrayList<String>();
		Scanner sc = new Scanner(System.in);
		do {
			System.out.println("Introdueix una cadena (deixa buit per acabar): ");
			cadena = sc.nextLine();
			if (!cadena.equals(""))
				llista.add(cadena);
		} while (!cadena.equals(""));
		do {
			System.out.println("Introdueix una cadena (deixa buit per acabar): ");
			cadena = sc.nextLine();
			Iterator<String> it = llista.iterator();
			String cadena2;
			boolean paraulaTrobada = false;
			while (it.hasNext() && !paraulaTrobada) {
				cadena2 = it.next();
				if (cadena.equals(cadena2)) {
					paraulaTrobada = true;
				}
			}
			if (paraulaTrobada) {
				System.out.println("La paraula " + cadena + " l'has introduit abans.");
			} else {
				System.out.println("La paraula " + cadena + " no l'has introduit abans.");
			}
		} while (!cadena.equals(""));
		sc.close();
	}

}
