package exercici2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		List<Integer> llista = new ArrayList<Integer>();
		llista.add(6);
		llista.add(5);
		llista.add(4);
		llista.add(3);
		if (OrdenadaMajorMenor(llista))
			System.out.println("La llista esta ordenada de major a menor");
		else
			System.out.println("La llista no esta ordenada de major a menor");

	}

	public static boolean OrdenadaMajorMenor(List<Integer> llista) {
		Iterator<Integer> it = llista.iterator();
		int numAnterior = llista.get(0);
		int numActual;
		while (it.hasNext()) {
			numActual = it.next();
			if (numAnterior < numActual)
				return false;
			numAnterior = numActual;
		}

		return true;

	}

}
