package exercici7;

public class Main {

	public static void main(String[] args) {

		LlistaOrdenada num = new LlistaOrdenada();
		num.afegirInteger(7);
		num.afegirInteger(3);
		num.afegirInteger(13);
		num.afegirInteger(15);
		num.afegirInteger(2);
		
		num.afegirIntegerPerCercaBinaria(5);
		num.afegirIntegerPerCercaBinaria(13);
		num.afegirIntegerPerCercaBinaria(4);
		num.afegirIntegerPerCercaBinaria(5);
		num.afegirIntegerPerCercaBinaria(6);
		System.out.println("Llista ordenada amb CercaBinaria i sense cerca.");
		System.out.println();
		for (int i = 0; i < num.llistaOrdenada.size(); i++) {
			System.out.print(num.llistaOrdenada.get(i)+" ");
		}
		System.out.println();
		num.eliminarIntegerPerCercaBinaria(13);
		num.eliminarIntegerPerCercaBinaria(5);
		num.eliminarIntegerPerCercaBinaria(9);
		System.out.println();
		System.out.println("Llista ordenada amb numeros esborrats amb CercaBinaria.");
		System.out.println();
		for (int i = 0; i < num.llistaOrdenada.size(); i++) {
			System.out.print(num.llistaOrdenada.get(i)+" ");
		}
		System.out.println();
		// Busqueda del numero a la llista
		int num2=num.cercarInteger(6);
		System.out.println();
		if(num2<0){
			System.err.println("Aquest numero no esta a la llista.");
		}
		else{
			num2=num.llistaOrdenada.get(num2);
			System.out.println("Numero "+num2+" trobat.");
		}

	}

}
