package exercici7;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class LlistaOrdenada {

	List<Integer> llistaOrdenada = new ArrayList<Integer>();

	public void afegirInteger(int num) {
		ListIterator<Integer> it = llistaOrdenada.listIterator();
		int posicio = 0;
		while (it.hasNext()) {
			if (num > it.next()) {
				posicio = it.nextIndex();
			}
		}
		llistaOrdenada.add(posicio, num);
	}

	public void eliminarInteger(int num) {
		Iterator<Integer> it = llistaOrdenada.iterator();
		while (it.hasNext()) {
			if (it.next() == num) {
				it.remove();
			}
		}
	}
	
	public void afegirIntegerPerCercaBinaria(int num) {
		int posicio = cercarInteger(num);
		if (posicio < 0) {
			llistaOrdenada.add((-posicio), num);
		} else {
			llistaOrdenada.add(posicio, num);
		}
	}
	
	public void eliminarIntegerPerCercaBinaria(int num) {
		int posicio = cercarInteger(num);
		if(posicio > 0){
			llistaOrdenada.remove(posicio);
		}
	}

	public int cercarInteger(int num) {
		int posicio = -1;
		int minExtrem = 0;
		int maxExtrem = llistaOrdenada.size() - 1;
		int interval = 0;
		boolean numTrobat = false;
		while (minExtrem <= maxExtrem && !numTrobat) {
			if (llistaOrdenada.get(interval) == num) {
				posicio = interval;
				numTrobat = true;
			} else if (llistaOrdenada.get(interval) > num) {
				maxExtrem = interval - 1;
			} else if (llistaOrdenada.get(interval) < num) {
				minExtrem = interval + 1;
			}
			interval = (minExtrem + maxExtrem) / 2;
		}
		if (posicio == -1 && !numTrobat) {
			posicio = -(interval + 1);
		}
		return posicio;
	}
}