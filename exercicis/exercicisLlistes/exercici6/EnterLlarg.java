package exercici6;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class EnterLlarg {
	List<Integer> arrayNum = new ArrayList<Integer>();

	public EnterLlarg(long numLong) {
		String num = Long.toString(numLong);
		for (int i = 0; i < num.length(); i++) {
			arrayNum.add(Integer.parseInt(String.valueOf(num.charAt(i))));
		}
	}

	public EnterLlarg(byte[] vectorBytes) {
		Byte b;
		for (int i = 0; i < vectorBytes.length; i++) {
			b = vectorBytes[i];
			arrayNum.add(b.intValue());
		}

	}

	public EnterLlarg(String numString) {
		for (int i = 0; i < numString.length(); i++) {
			arrayNum.add(Integer.parseInt(String.valueOf(numString.charAt(i))));
		}
	}

	public EnterLlarg() {
	}

	@Override
	public String toString() {
		String num = "";
		Iterator<Integer> it = arrayNum.iterator();
		while (it.hasNext()) {
			num += it.next();
		}

		return num;
	}

	public EnterLlarg sumaEnters(EnterLlarg num2) {
		EnterLlarg num3 = new EnterLlarg();
		ListIterator<Integer> granIt, petitIt;
		int mod, quocient = 0, numLlistaGran, numLlistaPetita;
		if (arrayNum.size() > num2.arrayNum.size()) {
			granIt = arrayNum.listIterator(arrayNum.size());
			petitIt = num2.arrayNum.listIterator(num2.arrayNum.size());
		} else {
			granIt = num2.arrayNum.listIterator(num2.arrayNum.size());
			petitIt = arrayNum.listIterator(arrayNum.size());
		}

		while (petitIt.hasPrevious()) {
			numLlistaGran = granIt.previous();
			numLlistaPetita = petitIt.previous();
			if (numLlistaGran + numLlistaPetita >= 10) {
				mod = (numLlistaGran + numLlistaPetita) % 10;
				num3.arrayNum.add(0, mod + quocient);
				quocient = 1;
			} else {
				if (quocient == 1) {
					num3.arrayNum.add(0, numLlistaGran + numLlistaPetita + quocient);
					quocient=0;
				} else {
					num3.arrayNum.add(0, numLlistaGran + numLlistaPetita);
				}
			}
		}
		while (granIt.hasPrevious()) {
			numLlistaGran = granIt.previous();
			if (numLlistaGran >= 10) {
				mod = numLlistaGran % 10;
				num3.arrayNum.add(0, mod + quocient);
				quocient = 1;

			} else {
				if (quocient == 1) {
					num3.arrayNum.add(0, numLlistaGran + quocient);
					quocient=0;
				} else {
					num3.arrayNum.add(0, numLlistaGran);
				}
			}
		}

		if (quocient == 1 && !granIt.hasPrevious()) {
			num3.arrayNum.add(0, quocient);
		}
		return num3;
	}
}
