package exercici6;

public class Main {

	public static void main(String[] args) {
		long a = 5678;
		byte[] b = { 1, 2, 3, 4, 5, 6, 7, 3, 4, 5, 6, 7, 8, 9, 2, 3, 4, 5, 6, 7, 8, 9, 4, 5, 6, 7, 8, 9 };

		EnterLlarg num1 = new EnterLlarg(a);
		EnterLlarg num2 = new EnterLlarg("5432487654321987654321321");
		EnterLlarg num3 = new EnterLlarg(b);
		EnterLlarg num4;

		num4 = num1.sumaEnters(num2);
		System.out.println("Numero 1: " + num1);
		System.out.println("Numero 2: " + num2);
		System.out.println("Numero 3: " + num3);
		System.out.println("Suma Numero 1 i Numero 2: " + num4);
		System.out.println();

		num4 = num2.sumaEnters(num3);
		System.out.println("Numero 1: " + num1);
		System.out.println("Numero 2: " + num2);
		System.out.println("Numero 3: " + num3);
		System.out.println("Suma Numero 2 i Numero 3: " + num4);
		System.out.println();

		num4 = num1.sumaEnters(num3);
		System.out.println("Numero 1: " + num1);
		System.out.println("Numero 2: " + num2);
		System.out.println("Numero 3: " + num3);
		System.out.println("Suma Numero 1 i Numero 3: " + num4);
		System.out.println();

	}

}
