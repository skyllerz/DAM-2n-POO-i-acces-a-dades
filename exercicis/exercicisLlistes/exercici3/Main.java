package exercici3;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		List<String> llista = new ArrayList<String>();
		String[] llenguatjes = { "Java", "C", "C++", "Visual Basic", "PHP", "Python", "Perl", "C#", "JavaScript",
				"Delphi" };
		for (int i = 0; i < llenguatjes.length; i++) {
			llista.add(llenguatjes[i]);
		}
		System.out.println("* El llenguatge m�s popular de la llista.");
		System.out.println(llista.get(0));
		System.out.println("* El sis� m�s popular.");
		System.out.println(llista.get(5));
		System.out.println("* Els tres m�s populars.");
		for (int i = 0; i < 3; i++)
			System.out.print(llista.get(i) + " ");
		System.out.println();
		System.out.println("* Els tres menys populars.");
		for (int i = llista.size() - 3; i < llista.size(); i++)
			System.out.print(llista.get(i) + " ");
		System.out.println();
		System.out.println("* Tots menys el primer.");
		for (int i = 1; i < llista.size(); i++)
			System.out.print(llista.get(i) + " ");
		System.out.println();
		System.out.println("* Imprimeix la llista amb cada llenguatge en una l�nia i numerada.");
		for (int i = 0; i < llista.size(); i++)
			System.out.println((i + 1) + " - " + llista.get(i));
	}

}