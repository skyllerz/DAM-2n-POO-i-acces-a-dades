package exercici2;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Main {

	public static void main(String[] args) {
		Set<Integer> divisorsNum = new HashSet<Integer>();
		Set<Integer> divisorsNum2 = new HashSet<Integer>();
		Set<Integer> noDivisorsNumNum2 = new HashSet<Integer>();

		int num, num2;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix un numero: ");
		num = sc.nextInt();
		sc.nextLine();
		System.out.println("Introdueix un segon numero: ");
		num2 = sc.nextInt();
		sc.nextLine();
		sc.close();
		divisorsNum = trobarDivisors(num);
		divisorsNum2 = trobarDivisors(num2);

		System.out.println("\nNombres del 2 al 1000 que siguin divisors tant de " + num + " com de " + num2 + " (sense repetir nombres)\n");
		divisorsNum.retainAll(divisorsNum2);
		System.out.println(divisorsNum);
		System.out.println("\nNombres del 2 al 1000 que siguin divisors de " + num + " o de " + num2 + " (sense repetir nombres)\n");
		divisorsNum.addAll(divisorsNum2);
		System.out.println(divisorsNum);
		System.out.println("\nNombres del 2 al 100 que no siguin divisors ni de " + num + " ni de " + num2 + "  (sense repetir nombres)\n");
		for (int i = 2; i <= 100; i++) {
			noDivisorsNumNum2.add(i);
		}
		noDivisorsNumNum2.removeAll(divisorsNum);
		noDivisorsNumNum2.removeAll(divisorsNum2);
		System.out.println(noDivisorsNumNum2);
	}

	public static Set<Integer> trobarDivisors(int num) {
		Set<Integer> divisors = new HashSet<Integer>();
		for (int i = 2; i < 1000; i++) {
			if (num % i == 0) {
				divisors.add(i);
			}

		}
		return divisors;
	}

}
