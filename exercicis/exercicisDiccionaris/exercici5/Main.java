package exercici5;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Map<String, String> paisCapital = new HashMap<String, String>();
		Scanner sc = new Scanner(System.in);
		String pais, capital;
		paisCapital.put("Espanya", "Madrid");
		paisCapital.put("Fran�a", "Par�s");
		paisCapital.put("It�lia", "Roma");
		paisCapital.put("Anglaterra", "Londres");
		paisCapital.put("Alemanya", "Berl�n");
		System.out.println("Introdueix un nom de pa�s: ");
		pais = sc.nextLine();
		while (!pais.equals("")) {
			if (paisCapital.containsKey(pais)) {
				System.out.println("La capital de " + pais + " �s " + paisCapital.get(pais));
				System.out.println();
			} else {
				System.out.println("Quina �s la capital de " + pais + "? ");
				capital = sc.nextLine();
				if (capital.equals("")) {
					System.err.println("No s'admet una capital en blanc.");
				} else {
					paisCapital.put(pais, capital);
				}
			}
			System.out.println("Introdueix un nom de pa�s: ");
			pais = sc.nextLine();
		}
		sc.close();
	}

}
