package exercici3;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Main {

	public static void main(String[] args) {
		Map<String, Integer> llistaCompra = new HashMap<String, Integer>();
		Set<String> claus = llistaCompra.keySet();
		Scanner sc = new Scanner(System.in);
		String ingredient;
		int totalIngredients = 0, quantitatIngredient;
		llistaCompra.put("Tom�quet", 6);
		llistaCompra.put("Flam", 4);
		llistaCompra.put("Pizza", 2);
		llistaCompra.put("Llauna de tonyina", 2);
		llistaCompra.put("Blat de moro", 5);
		llistaCompra.put("Enciam", 1);
		System.out.println();

		// * A partir del diccionari, imprimeix nom�s la llista d'ingredients

		System.out.println();
		System.out.println(llistaCompra.keySet());
		System.out.println();

		// * Imprimeix la quantitat de tom�quets.");

		System.out.println();
		System.out.println(llistaCompra.get("Tom�quet"));
		System.out.println();

		// * Imprimeix aquells ingredients dels quals n'h�gim de comprar m�s de
		// 3 unitats.

		System.out.println();
		for (String clau : claus) {
			if (llistaCompra.get(clau) > 3) {
				System.out.println(clau + ": " + llistaCompra.get(clau));
			}
		}
		System.out.println();

		// * Pregunta a l'usuari un ingredient i retorna el n�mero d'unitats que
		// s'han de comprar. Si l'ingredient no �s a la llista de la compra,
		// retorna un missatge d'error

		System.out.println();
		System.out.println("Llista d'ingredients: " + llistaCompra.keySet());
		System.out.println();
		System.out.println("Introdueix un ingredient: ");
		ingredient = sc.next();
		sc.nextLine();
		System.out.println();
		if (llistaCompra.containsKey(ingredient)) {
			System.out.println("Ingredient:" + ingredient + " Quantitat: " + llistaCompra.get(ingredient));
		} else {
			System.err.println("Aquest ingredient no esta a la llista.");
		}
		System.out.println();

		// * Imprimeix tota la llista amb aquest format:
		// 'ingredient (unitats a comprar)'

		for (String clau : claus) {
			System.out.println(clau + " (" + llistaCompra.get(clau) + ")");
		}
		System.out.println();

		// * A partir del diccionari, sumar totes les quantitats i donar el
		// n�mero total d'�tems que hem de comprar. Resultat: 20

		System.out.println();
		for (String clau : claus) {
			totalIngredients += llistaCompra.get(clau);
		}
		System.out.println("Total d'ingredients: " + totalIngredients);
		System.out.println();

		// * Pregunta a l'usuari un ingredient, despr�s demana-li un nou valor
		// d'unitats i modifica l'entrada corresponent al diccionari
		System.out.println();
		System.out.println("Llista d'ingredients: " + llistaCompra.keySet());
		System.out.println();
		System.out.println("Introdueix l'ingredient a modificar: ");
		ingredient = sc.next();
		sc.nextLine();
		System.out.println();
		if (llistaCompra.containsKey(ingredient)) {
			System.out.println("Ingredient:" + ingredient + " Quantitat: " + llistaCompra.get(ingredient));
			System.out.println();
			System.out.println("Introdueix una nova quantitat de " + ingredient + ": ");
			quantitatIngredient = sc.nextInt();
			sc.nextLine();
			System.out.println();
			llistaCompra.put(ingredient,quantitatIngredient);
			System.out.println("Ingredient Actualitzat");
			System.out.println();
			System.out.println("Ingredient:" + ingredient + " Quantitat: " + llistaCompra.get(ingredient));
		} else {
			System.err.println("Aquest ingredient no esta a la llista.");
		}
		
		System.out.println();
		sc.close();
	}

}
