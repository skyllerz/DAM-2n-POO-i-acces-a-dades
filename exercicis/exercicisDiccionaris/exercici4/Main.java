package exercici4;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Main {

	private static Map<String, String> programesVersions = new HashMap<String, String>();
	private static Set<String> nomProgrames = programesVersions.keySet();
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		int opcio;
		afegirProgramesVersions();
		do {
			menu();
			System.out.println("Que vols fer?");
			opcio = sc.nextInt();
			sc.nextLine();
			escollirOpcio(opcio);
		} while (opcio != 4);
		sc.close();
	}

	private static void afegirProgramesVersions() {
		programesVersions.put("Firefox".toLowerCase(), "3.5");
		programesVersions.put("Premiere".toLowerCase(), "CS3");
		programesVersions.put("After Effects".toLowerCase(), "CS3");
		programesVersions.put("Photoshop".toLowerCase(), "CS4");
		programesVersions.put("Gimp".toLowerCase(), "2.0.1");
		programesVersions.put("Python".toLowerCase(), "2.6.2");
		programesVersions.put("SnagIt".toLowerCase(), "7.0.1");
		System.out.println("Llista afegida:" + programesVersions);
	}

	private static void menu() {
		System.out.println("\n1. Mostrar llista");
		System.out.println("2. Consultar versi�");
		System.out.println("3. Afegir un nou programa");
		System.out.println("4. Sortir\n");
	}

	private static void escollirOpcio(int opcio) {
		switch (opcio) {
		case 1:
			for (String nomPrograma : nomProgrames) {
				System.out.println(nomPrograma);
			}
			break;
		case 2:
			mostrarVersio();
			break;
		case 3:
			afegirPrograma();
			break;
		case 4:
			System.out.println("Sortint del programa...");
			break;
		default:
			System.err.println("Opci� incorrecta");
			break;
		}
	}

	private static void afegirPrograma() {
		String programa, versio;
		System.out.println("Introdueix el nom del programa: ");
		programa = sc.next().toLowerCase();
		sc.nextLine();

		if (!programesVersions.containsKey(programa)) {
			System.out.println("Introdueix la seva versi�: ");
			versio = sc.next();
			sc.nextLine();
			programesVersions.put(programa, versio);
		} else {
			System.err.println("Aquest programa ja existeix");
		}
	}

	private static void mostrarVersio() {
		String programa;
		System.out.println("Introdueix el nom del programa: ");
		programa = sc.next().toLowerCase();
		sc.nextLine();
		if (!programesVersions.containsKey(programa)) {
			System.err.println("Aquest programa no esta a la llista o no l'has escrit correctament");

		} else {
			System.out.println("Versi� del programa " + programa + ": " + programesVersions.get(programa));
		}
	}
}
