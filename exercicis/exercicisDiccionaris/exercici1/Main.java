package exercici1;

import java.util.HashSet;
import java.util.Set;

public class Main {

	public static void main(String[] args) {

		Set<String> conjuntStrings = new HashSet<String>();
		String frase = "Aix� �s una l�nia de text de prova, prova �s aix�, una l�nia i nom�s una l�nia";

		// Afegim les paraules en una array de Strings

		String[] paraules = frase.split(" ");
		int contador = 0;

		// Les treiem qualsevol caracter que ens molesti ( , ) ho posem en
		// majuscula tot, i ho guardem al conjunt

		for (String paraula : paraules) {
			paraula = paraula.replaceAll(",", "").toUpperCase();
			conjuntStrings.add(paraula);

		}

		// Per cada String del conjunt recorrem tota l'array de Strings i contem
		// les vegades que ha sortit

		for (String string : conjuntStrings) {
			contador = 0;
			for (String paraula : paraules) {
				if (string.equals(paraula.toUpperCase())) {
					contador++;
				}
			}
			System.out.println(string + " (" + contador + ")");
		}
	}

}
