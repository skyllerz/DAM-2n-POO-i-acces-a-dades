package aggregationFramework;

import static com.mongodb.client.model.Accumulators.avg;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.sort;
import static com.mongodb.client.model.Filters.gte;
import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Sorts.orderBy;
import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;

public class Exercici1 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("db");
		MongoCollection<Document> collection = db.getCollection("students");
		Bson filterExam = new Document("scores.type", "exam");
		Bson filterAVG = gte("totalScore", 40);
		Bson sort = orderBy(descending("totalScore"));
		List<Document> results = collection
				.aggregate(asList(Aggregates.unwind("$scores"), match(filterExam),
						group("$name", avg("totalScore", "$scores.score")), match(filterAVG), sort(sort)))
				.into(new ArrayList<Document>());
		for (Document doc : results) {
			System.out.println(doc.toJson());
		}

		client.close();
	}

}
