package aggregationFramework;

import static com.mongodb.client.model.Accumulators.avg;
import static com.mongodb.client.model.Aggregates.group;
import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;

public class Exercici3 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("db");
		MongoCollection<Document> collection = db.getCollection("students");
		List<Document> results = collection
				.aggregate(
						asList(Aggregates.unwind("$scores"), group("$scores.type", avg("avgScores", "$scores.score"))))
				.into(new ArrayList<Document>());
		for (Document doc : results) {
			System.out.println(doc.toJson());
		}

		client.close();
	}

}
