package aggregationFramework;

import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.project;
import static com.mongodb.client.model.Aggregates.unwind;
import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Exercici2 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("db");
		MongoCollection<Document> collection = db.getCollection("students");
		Bson project = new Document("name", "$name").append("type", "$scores.type");
		List<Document> results = collection
				.aggregate(asList(unwind("$scores"), project(project),
						group(new Document("name", "$name").append("type", "$type"), sum("type", 1))))
				.into(new ArrayList<Document>());
		for (Document doc : results) {
			System.out.println(doc.toJson());
		}

		client.close();
	}

}
