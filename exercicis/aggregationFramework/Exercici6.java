package aggregationFramework;

import static com.mongodb.client.model.Accumulators.avg;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.project;
import static com.mongodb.client.model.Aggregates.sort;
import static com.mongodb.client.model.Aggregates.unwind;
import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Sorts.orderBy;
import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Exercici6 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("db");
		MongoCollection<Document> collection = db.getCollection("grades");
		Bson filterExam = new Document("type", "exam");
		Bson sort = orderBy(descending("AvgScore"));
		Bson project = new Document("class_id", "$class_id").append("type", "$scores.type").append("score",
				"$scores.score");
		List<Document> result = collection.aggregate(asList(unwind("$scores"), project(project), match(filterExam),
				group("$class_id", avg("AvgScore", "$score")), sort(sort))).into(new ArrayList<Document>());
		for (Document doc : result) {
			System.out.println(doc.toJson());
		}
		System.out.println("La clase " + result.get(0).getInteger("_id") + " t� la mitjana d'ex�mens m�s alta: "
				+ result.get(0).getDouble("AvgScore"));

		client.close();
	}

}
