package aggregationFramework;

import static com.mongodb.client.model.Accumulators.avg;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.limit;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.project;
import static com.mongodb.client.model.Aggregates.skip;
import static com.mongodb.client.model.Aggregates.unwind;
import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;

public class Exercici5 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("db");
		MongoCollection<Document> collection = db.getCollection("students");
		Scanner sc = new Scanner(System.in);
		System.out.print("Introdueix el nom de l'estudiant: ");
		String nom = sc.nextLine();

		Bson filterExam = new Document("scores.type", "exam").append("name", nom);
		Bson filterQuiz = new Document("scores.type", "quiz").append("name", nom);
		Bson filterHomework = new Document("scores.type", "homework").append("name", nom);
		Bson project = new Document("name", "$name").append("type", "$scores.type").append("score", "$scores.score");
		int numPersona = 1;
		List<Document> contador = collection.aggregate(
				asList(match(new Document("name", nom)), Aggregates.group("$name", Accumulators.sum("count", 1))))
				.into(new ArrayList<Document>());
		List<Document> mostrarPersones = collection.aggregate(asList(match(new Document("name", nom))))
				.into(new ArrayList<Document>());
		int cont = contador.get(0).getInteger("count");
		if (cont > 1) {
			for (Document doc : mostrarPersones) {

				System.out.println(doc.toJson());
			}
			do {
				System.out.print("Hi han " + cont + " persones amb aquest nom, quina vols (1-" + cont + ")? -");
				numPersona = sc.nextInt();
			} while (numPersona > cont);
		}
		sc.close();
		List<Document> dadesPersona = collection.aggregate(asList(match(new Document("name", nom)),
				skip(numPersona - 1), limit(1), unwind("$scores"), project(project))).into(new ArrayList<Document>());
		for (Document doc : dadesPersona) {

			System.out.println(doc.toJson());
		}
		List<Document> mitjanaExamens = collection.aggregate(asList(match(filterExam), skip(numPersona - 1), limit(1),
				unwind("$scores"), group("$_id", avg("Mitjana examens", "$scores.score"))))
				.into(new ArrayList<Document>());
		List<Document> mitjanaQuiz = collection.aggregate(asList(match(filterQuiz), skip(numPersona - 1), limit(1),
				unwind("$scores"), group("_id", avg("Mitjana quiz", "$scores.score")))).into(new ArrayList<Document>());
		List<Document> mitjanaTests = collection.aggregate(asList(match(filterHomework), skip(numPersona - 1), limit(1),
				unwind("$scores"), group("$name", avg("Mitjana tests", "$scores.score"))))
				.into(new ArrayList<Document>());
		Exercici5 ex = new Exercici5();
		Double notaFinal;
		for (Document doc : mitjanaExamens) {

			System.out.println(doc.toJson());
		}
		for (int i = 0; i < mitjanaExamens.size(); i++) {
			notaFinal = ex.calcularNota(mitjanaExamens.get(i).getDouble("Mitjana examens"),
					mitjanaQuiz.get(i).getDouble("Mitjana quiz"), mitjanaTests.get(i).getDouble("Mitjana tests"));
			System.out.println("Id: " + mitjanaExamens.get(i).getInteger("_id") + " --- Nom: " + nom
					+ " --- Nota Final: " + notaFinal);
		}

		client.close();
	}

	public Double calcularNota(Double mitjanaExamen, Double mitjanaDeures, Double mitjanaTest) {
		Double notaFinal = (mitjanaExamen * 60 / 100) + (mitjanaDeures * 30 / 100) + (mitjanaTest * 10 / 100);
		if (notaFinal < 40)
			return 40.0;
		else
			return notaFinal;

	}
}
