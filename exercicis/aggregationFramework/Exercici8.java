package aggregationFramework;

import static com.mongodb.client.model.Aggregates.project;
import static com.mongodb.client.model.Aggregates.sort;
import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Sorts.orderBy;
import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Exercici8 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("db");
		MongoCollection<Document> collection = db.getCollection("grades");
		Bson sort = orderBy(ascending("class_id"));
		Bson project = new Document("class_id", "$class_id").append("student_id", "$student_id");
		List<Document> result = collection.aggregate(asList(project(project), sort(sort)))
				.into(new ArrayList<Document>());
		for (Document doc : result) {
			System.out.println(
					"ID Clase: " + doc.getInteger("class_id") + " --- ID Estudiant: " + doc.getInteger("student_id"));
		}

		client.close();
	}

}
