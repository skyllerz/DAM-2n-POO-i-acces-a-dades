package aggregationFramework;

import static com.mongodb.client.model.Accumulators.avg;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.unwind;
import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Exercici4 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("db");
		MongoCollection<Document> collection = db.getCollection("students");
		Bson filterExam = new Document("scores.type", "exam");
		Bson filterQuiz = new Document("scores.type", "quiz");
		Bson filterHomework = new Document("scores.type", "homework");
		List<Document> results = collection.aggregate(
				asList(unwind("$scores"), match(filterExam), group("$name", avg("Mitjana examens", "$scores.score"))))
				.into(new ArrayList<Document>());
		List<Document> results2 = collection.aggregate(
				asList(unwind("$scores"), match(filterQuiz), group("$name", avg("Mitjana quiz", "$scores.score"))))
				.into(new ArrayList<Document>());
		List<Document> results3 = collection.aggregate(
				asList(unwind("$scores"), match(filterHomework), group("$name", avg("Mitjana tests", "$scores.score"))))
				.into(new ArrayList<Document>());
		Exercici4 ex = new Exercici4();
		Double notaFinal;
		for (int i = 0; i < results.size(); i++) {
			notaFinal = ex.calcularNota(results.get(i).getDouble("Mitjana examens"),
					results2.get(i).getDouble("Mitjana quiz"), results3.get(i).getDouble("Mitjana tests"));
			System.out.println("Nom: " + results.get(i).getString("_id") + " --- Nota Final: " + notaFinal);
		}
		client.close();
	}

	public Double calcularNota(Double mitjanaExamen, Double mitjanaDeures, Double mitjanaTest) {
		Double notaFinal = (mitjanaExamen * 60 / 100) + (mitjanaDeures * 30 / 100) + (mitjanaTest * 10 / 100);
		if (notaFinal < 40)
			return 40.0;
		else
			return notaFinal;

	}
}
