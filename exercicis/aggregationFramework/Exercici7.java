package aggregationFramework;

import static com.mongodb.client.model.Accumulators.avg;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.project;
import static com.mongodb.client.model.Aggregates.sort;
import static com.mongodb.client.model.Aggregates.unwind;
import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Sorts.orderBy;
import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Exercici7 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("db");
		MongoCollection<Document> collection = db.getCollection("grades");
		Scanner sc = new Scanner(System.in);
		System.out.print("Introdueix el codi de l'estudiant: ");
		int codiEstudiant = sc.nextInt();
		sc.close();
		Bson sort = orderBy(ascending("class_id"));
		Bson project = new Document("student_id", "$student_id").append("class_id", "$class_id")
				.append("type", "$scores.type").append("score", "$scores.score");
		Bson match = new Document("student_id", codiEstudiant);
		List<Document> result = collection.aggregate(asList(match(match), unwind("$scores"), project(project),
				group("$type", avg("AvgScore", "$score")), sort(sort))).into(new ArrayList<Document>());
		//
		for (Document doc : result) {
			System.out.println("ID Estudiant: " + codiEstudiant + " --- Type: " + doc.getString("_id")
					+ " --- AvgScore: " + doc.getDouble("AvgScore"));
		}
		client.close();
	}

}
