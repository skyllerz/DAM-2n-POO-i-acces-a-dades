package beans;


public class ActorFilm {
	private String firstName;
	private String peliculas;
	
	public String getPeliculas() {
		return peliculas;
	}
	public void setPeliculas(String peliculas) {
		this.peliculas = peliculas;
	}
	public ActorFilm(String firstName, String peliculas) {
		this.firstName = firstName;
		this.peliculas = peliculas;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	

}
