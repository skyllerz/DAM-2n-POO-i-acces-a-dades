package accessBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.ActorFilm;
import beans.Film;

public class OperacionsBD {
	protected Connection connexio = null;
	protected Statement st = null;
	protected Statement st2 = null;
	protected ResultSet rs = null;
	protected ResultSet rs2 = null;

	protected Connection getConnection() throws SQLException, ClassNotFoundException {
		if (connexio == null) {
			Class.forName("com.mysql.jdbc.Driver");
			connexio = DriverManager.getConnection("jdbc:mysql://localhost/sakila", "root", "12345");
		}
		return connexio;
	}

	protected void closeConnection() {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				;
			} finally {
				rs = null;
			}
		}
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				;
			} finally {
				st = null;
			}
		}
		if (rs2 != null) {
			try {
				rs2.close();
			} catch (SQLException e) {
				;
			} finally {
				rs2 = null;
			}
		}
		if (st2 != null) {
			try {
				st2.close();
			} catch (SQLException e) {
				;
			} finally {
				st2 = null;
			}
		}
		if (connexio != null) {
			try {
				connexio.close();
			} catch (SQLException e) {
				;
			} finally {
				connexio = null;
			}
		}
	}

	public List<Film> consultaPellicules() throws SQLException, ClassNotFoundException {
		List<Film> pellicules = new ArrayList<Film>();
		try {
			connexio = getConnection();
			st = connexio.createStatement();
			String sql = "select * from film;";
			rs = st.executeQuery(sql);

			while (rs.next()) {
				Film f = new Film(rs.getInt("film_id"), rs.getString("title"), rs.getString("description"),
						rs.getString("release_year"), rs.getInt("language_id"), rs.getInt("original_language_id"),
						rs.getInt("length"));
				pellicules.add(f);
			}
		} finally {
			closeConnection();
		}
		return pellicules;
	}

	public List<ActorFilm> consultaActors() throws SQLException, ClassNotFoundException {
		List<ActorFilm> actorPellicula = new ArrayList<ActorFilm>();
		try {
			connexio = getConnection();
			st = connexio.createStatement();
			st2 = connexio.createStatement();
			String sql = "select actor.first_name,film.title from actor "
					+ "JOIN film_actor ON actor.actor_id = film_actor.actor_id "
					+ "JOIN film ON film_actor.film_id = film.film_id " + "GROUP BY actor.first_name";
			rs = st.executeQuery(sql);

			while (rs.next()) {
				String pellicules = "";
				String nomActor = rs.getString("actor.first_name");
				String sql2 = "select actor.first_name, film.title from actor "
						+ "JOIN film_actor ON actor.actor_id = film_actor.actor_id "
						+ "JOIN film ON film_actor.film_id = film.film_id " + "WHERE actor.first_name LIKE '" + nomActor
						+ "'";
				rs2 = st2.executeQuery(sql2);
				while (rs2.next()) {
					pellicules += rs2.getString("film.title") + ", ";
				}
				pellicules = pellicules.substring(0, pellicules.length() - 1);
				ActorFilm f = new ActorFilm(nomActor, pellicules);
				actorPellicula.add(f);

			}
		} finally {
			closeConnection();
		}
		return actorPellicula;
	}
}
