<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="beans.ActorFilm,java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Llista d'actors</title>
</head>
<body>
<h1>Llista d'actors</h1>
<table border='1'>
<tr><th>Nom Actor</th><th>Pel·lícules</th></tr>
<%
@SuppressWarnings("unchecked")
List<ActorFilm> pellicules = (List<ActorFilm>) request.getAttribute("actors");
for (ActorFilm f : pellicules) {%>
	<tr><td><%=f.getFirstName() %></td>
	<td><%=f.getPeliculas() %></td></tr>
<%}%>
</table>
<a href="index.html">Inici</a>
</body>
</html>