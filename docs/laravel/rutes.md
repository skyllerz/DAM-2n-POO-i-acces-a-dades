## Enrutament a Laravel

En un framework web totes les peticions contra la web van sempre al fitxer
inicial, en aquest cas `public/index.php`.

D'aquesta manera, el framework pot carregar la configuració i incloure les
classes bàsiques que es necessiten.

A partir d'aquí, es consulta la resta de la ruta sol·licitada i els seus
paràmetres. Existeix un punt del framework on podem configurar quines rutes
van a parar a quines classes i mètodes. Això és molt útil, perquè ens
independitza la ruta utilitzada dels noms interns que utilitzem al nostre
programa.

A més, això ens permet agrupar moltes rutes diferents i passar determinats
filtres a aquestes rutes sense haver de repetir codi o haver-lo d'incloure
al mètode que resol finalment la petició.

En Laravel el fitxer que ens permet configurar l'enrutament és
`app/Http/routes.php`.

Aquesta és la ruta d'exemple que ve per defecte després de la instal·lació:

```php5
Route::get('/', function () {
    return view('welcome');
});
```

Aquest codi indica que en resposta a la ruta arrel (`/`) s'ha d'executar la
funció que s'ha passat per paràmetre a *Route::get()*. Aquest tipus de
funcions s'anomenen *clousures*.

El que fa la funció és retornar la vista anomenada *welcome*, que podem
trobar a `resources/views/welcome.blade.php`.

La funció *view* és una funció global que troba definida a
`app/vendor/laravel/framework/src/Illuminate/Foundation/helpers.php`. El que
passa aquí és que se cerca la vista *welcome*, s'interpreta el llenguatge de
plantilles *blade* i es construeix l'HTML que es passa com a resposta a
l'usuari.

Més avall del fitxer *routes.php* tenim un altre exemple:

```php5
Route::group(['middleware' => ['web']], function () {
    //
});
```

Per a totes les rutes que s'especifiquin dins d'aquest *Route::group*
s'executarà el *middleware* anomenat *web* abans d'enrutar. Aquest *middleware*
està definit a `app/Http/Kernel.php`. El que aquest grup és mantenir les
variables de sessió: s'encarrega de transmetre la *cookie* de sessió,
amb protecció contra diversos atacs de rapte de sessions.

En definitiva, totes les rutes que depenguin de la sessió de l'usuari s'hauran
de posar dins d'aquest *Route::group*.

Esencialment ens interessaran dos mètodes de *Route*: *Route::get* i
*Route::post* que serviran, respectivament, per respondre a peticions HTTP
GET o POST.

### Rutes amb paràmetres

Un aspecte que ens dóna una gran potència al sistema d'enrutament és el de
les rutes amb paràmetres.

Dins d'una ruta podem definir un o més paràmetres que després recuperarem a
la funció de *callback*.

Per exemple, podem definir una ruta de la següent manera:

```php5
Route::get('user/{id}', function($id) {
  //
});
```

Aquesta ruta donarà resposta a totes les peticions dirigides a
`http://domini/user/` seguides de qualsevol paraula o nombre. Aquesta paraula
o nombre es rep com un paràmetre de la funció de *callback*, cosa que ens permet
adaptar la resposta. Per exemple, en aquest cas podríem mostrar la informació
de l'usuari amb un *id* concret.

Podem també rebre més d'un paràmetre:

```php5
Route::get('post/{post}/comment/{comment}', function ($postId, $commentId) {
  //
});
```

Els noms no són importants: el primer paràmetre de la ruta va amb el primer
paràmetre de la funció, i el segon amb el segon.

Les rutes també poden incloure paràmetres opcionals:

```php5
Route::get('user/{id?}', function($id=null) {
```

En aquest cas la ruta s'aplica tant si hi ha alguna cosa després de *user* com
si no. Si no hi ha res, el paràmetre *$id* rep el valor per defecte *null*.

### Rutes i models

Si tenim classes model de taules a una base de dades, podem automatitzar la
traducció d'identificadors a objectes del model.

Per exemple, podem passar l'identificador d'usuari a una ruta i que la
nostre funció de *callback* rebi directament un objecte amb les dades de
l'usuari que té aquest identificador.

Amb la instal·lació del Laravel tenim ja una classe model al fitxer
*app/User.php*. Aquesta classe està assignada al *namespace* *App*, així que
per fer-ne referència utilitzarem `App\User`.

En aquest segment de codi obtenim automàticament l'usuari corresponent a
l'identificador que hi hagi a la ruta:

```php5
Route::get('users/{user}', function (App\User $user) {
  return $user->email;
});
```

Aquí és important que el nom del model, *User*, coincideixi amb el nom que hem
posat a la ruta, *{user}*, per tal que la traducció sigui automàtica.

### Rutes i controladors

Habitualment la lògica de l'aplicació serà prou complexa com per no resoldre-ho
tot a la funció de *callback* de la ruta, sinó que utilitzarem classes
controladores.

Si ho fem així, podem assignar mètodes concrets que responguin a les peticions
contra una certa ruta:

```php5
Route::get('user/{id}', 'UserController@showProfile');
```

En aquest cas, en resposta a la petició s'executaria el mètode *showProfile*
de la classe *UserController*. Aquest mètode rebrà com a paràmetre l'*id*
que hi hagi a la ruta.

### Redireccions

En ocasions voldrem redirigir una petició a una adreça diferent.

Per exemple, si un usuari que no s'ha validat intenta accedir a una ruta
exclusiva per usuaris que ja s'han validat, voldrem redirigir-lo a una altra
ruta, per exemple, al formulari de *login*:

```php5
Route::get('garden', function() {
  if (Auth::guest())
    return Redirect::to('login');
  // Mostrar el jardí de l'usuari
});
```
