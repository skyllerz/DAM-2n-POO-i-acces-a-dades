## Lectura i escriptura de fitxers XML amb *DOM*

Per a crear i interpretar fitxers XML s'utilitza un *parser*: un processador
capaç de generar XML correcte i de validar un XML existent.

***TODO***: Millorar aquesta explicació:

Hi ha diversos processadors XML. En aquesta secció estudiarem el **DOM**. El
*DOM* interpreta un XML com un arbre on cada etiqueta és una branca més fina
de l'etiqueta anterior.

Quan treballem amb aquest sistema, tot el fitxer XML està guardat a memòria.
Això ens permet tenir una visió global del document, i navegar per ell tants
cops com necessitem, en qualsevol sentit, de forma ràpida.

A canvi, però, si el document és gran, ocuparà molta memòria.

### Creació d'un fitxer XML amb DOM

Per crear un fitxer XML nou procedirem en dos passos: primer crearem el
document en memòria, i després el guardarem en un fitxer.

El procediment és una mica complex. Java especifica unes API per treballar
amb XML a les anomenades **JAXP** (*Java API for XML Processing*). *JAXP*
especifica una sèrie d'interfícies i la funcionalitat que ha de proveir
cadascuna d'elles, però diversos fabricants poden proporcionar la seva
implementació. Part d'aquestes especificacions provenen de Java (als paquets
javax.xml.\*) i altres provenen directament de la W3C (als paquets
org.w3c.\*).

Els passos que hem de seguir són els següents:

- Obtenir un *DocumentFactory*. Aquest objecte permetrà crear documents XML i,
curiosament, per tal d'obtenir-lo, necessitarem primer un
*DocumentFactoryBuilder*, que permet crear objectes *DocumentFactory*.

- Crear un *Document* a partir del *DocumentFactory*.

- Crear l'element arrel i afegir-lo al *Document*.

- Crear la resta d'elements i atributs que vulguem i construir
l'estructura del document afegint uns elements als altres.

- En aquest punt ja tindrem l'arbre DOM a memòria, però ara necessitem
transformar-lo a un fitxer XML a disc. Per fer això necessitarem un
*Transformer*. Com abans, utilitzarem un *TransformerFactory* per crear el
nostre *Transformer*.

- Utilitzar el *Transformer* per convertir el DOM a un fitxer XML.

- Finalment, mostrarem l'arbre DOM per pantalla. Per fer això tornarem a
utilitzar el *Transformer* per convertir l'arbre DOM a memòria en un XML
a la consola.

[Exemple generador DOM](codi/fitxers/exemple_xml_dom/src/exemple_xml_dom/GeneradorDOM.java)

Analitzem el codi línia a línia:

**Línia 25**: creem un *DocumentBuilderFactory* amb el mètode estàtic
*newInstance()*. Després, creem un objecte *DocumentBuilder()*.

**Línia 26**: creem un nou *Document*.

**Línia 27**: assignem la versió d'XML que utilitzem.

**Línia 29**: creem l'element *empleats*.

**Línia 30**: afegim *empleats* com element arrel del document.

**Línia 32**: creem un element *empleat*.

**Línia 33**: assigna un atribut anomenat *id* amb valor 1 a l'empleat.

**Línia 34**: afegeix el nou *empleat* a l'element arrel del document.

**Línia 36**: creem un element *cognom*.

**Línia 37**: creem un element de text.

**Línia 38**: afegim l'element de text al *cognom*.

**Línia 39**: afegim el *cognom* a l'*empleat*.

**Línia 41**: especifiquem que l'origen de la transformació serà el nostre document.

**Línia 42**: especifiquem que el resultat de la transformació serà el fitxer
que hem preparat al principi.

**Línia 44**: obtenim una *TransformerFactory* i a partir d'ella un
*Transformer*.

**Línies 45 i 46**: indiquem que volem identar la sortida amb 5 espais.

**Línia 47**: fem la transformació. Aquí és on es genera el fitxer.

**Línia 49**: especifiquem la consola com un altre resultat per a la
transformació.

**Línia 50**: transformem el document en un fitxer XML que es mostra per
pantalla.

Al següent exemple hem recuperat la classe *Mascota* que hem utilitzat a
[fitxers d'objectes](fitxers_objectes.md).

Creem unes quantes mascotes i les guardem en un fitxer XML:

[Exemple generador DOM Objectes](codi/fitxers/exemple_xml_dom/src/exemple_xml_dom/GeneradorDOMObjectes.java)

Només hi ha un parell de novetats respecte l'exemple anterior:

**Línies 47 i 48**: hem fet un pas més i hem utilitzat un *DOMImplementation*.
Això ens permet afegir un espai de noms al nostre XML.

**Línies 74 a 79**: Hem implementat el mètode *crearElement()* per evitar
repetir codi.

**Línies 69 i 70**: Hem unit totes les excepcions i les hem transformades en
un sol tipus. Això ens simplifica la gestió d'excepcions en programes més
llargs. Amb `e.getCause()` podríem recuperar l'excepció original, que hem
passat al constructor de la nostra nova excepció.

**Línia 56**: Hem posat el nombre de potes com atribut. Veiem que la creació
d'un atribut és ben senzilla: només cal cridar el mètode *setAttribute()* de
*Element*.

### Lectura d'un fitxer XML amb DOM

Al següent exemple recuperem el fitxer que hem generat a l'exemple anterior i
refem la llista de mascotes:

[Exemple lector DOM Objectes](codi/fitxers/exemple_xml_dom/src/exemple_xml_dom/LectorDOMObjectes.java)

Comentem línia a línia:

**Línia 30**: el mètode *parse()* de *DocumentBuilder* llegeix el fitxer XML
complet i ens genera un *Document* amb la tota la informació que conté.

**Línia 33**: el mètode *getElementsByTag()* ens retorna tots elements XML
que es diuen com el text que li passem, en el mateix ordre com apareixen al
document. Ens retorna un *NodeList* que després podem recórrer.

**Línies 35 i 36**: recorrem la llista de mascotes.

**Línies 38 i 39**: ens assegurem que els nodes seleccionats són de tipus
element. En un fitxer XML podrien haver-hi altres tipus de node que
continguessin el text que hem cercat.

**Línies 41 a 44**: recuperem els valor que tenim guardats a l'XML per crear
un objecte *Mascota*.

**Línies 53**: hem creat aquest mètode útil per obtenir de forma senzilla els
elements fills de cada element *mascota*.

**Línia 54**: a partir de l'element (*<mascota>*) agafem tots els fills que
tenen l'etiqueta donada (per exemple, *<nom>*). Això ens retorna una llista
de nodes que només té un element. Agafem aquest element i obtenim la llista
dels seus nodes fills.

**Línies 55 i 56**: la llista de la línia anterior té un únic node (el text
corresponent a, per exemple, el nom de la mascota). Agafem aquest node i,
d'aquest node agafem el seu valor, és a dir, el text.
