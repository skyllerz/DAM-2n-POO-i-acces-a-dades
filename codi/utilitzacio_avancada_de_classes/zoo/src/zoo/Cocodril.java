package zoo;

public class Cocodril extends Animal {

	@Override
	public String alimenta(Zoo z) {
		String s = "Un cocodril busca a qui es pot menjar";
		Animal a = z.mostraAnimal();
		if (a != null && !a.equals(this)) {
			z.suprimeixAnimal(a);
			if (a instanceof Vaca)
				s = "Un cocodril es menja una vaca!";
			else if (a instanceof Cocodril)
				s = "Un cocodril es menja un altre cocodril";
		}
		return s;
	}

	@Override
	public String expressa(Zoo z) {
		return "Un cocodril obre una boca plena de dents";
	}

	@Override
	public String mou(Zoo z) {
		return "Un cocodril neda estany amunt estany avall";
	}

}