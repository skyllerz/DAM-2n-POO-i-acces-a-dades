package examen_fitxers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class MostraPoblacionsHandler extends DefaultHandler {
	private boolean titleObert = false;
	
	//Mètode de resposta a un esdeveniment de tipus 'inici d'element'
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes)
			throws SAXException {
		if (localName.equals("title")) {
			titleObert = true;
		} else if (localName.equals("Obs")) {
			// Comprova si es tracta de la població total
			if (attributes.getValue("SEX").equals("T")) {
				System.out.println(attributes.getValue("OBS_VALUE"));
			}
		}
	}
	
	//Mètode de resposta a un esdeveniment de tipus 'Fi de l'element'
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (localName.equals("title")) {
			titleObert = false;
		}
	}
	
	//Mètode de resposta a un esdeveniment de tipus 'caràcters dins d'element'
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		String contingut = new String(ch, start, length);
		contingut = contingut.replaceAll("[\t\n]", "").trim();
		if (!contingut.equals("") && titleObert) {
			System.out.print(contingut+": ");
		}
	}
}
