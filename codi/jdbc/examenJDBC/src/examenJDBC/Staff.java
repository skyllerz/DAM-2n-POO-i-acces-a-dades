package examenJDBC;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Staff {
	public final int id;
	public final String firstName;
	public final String lastName;
	
	public Staff(int id, String firstName, String lastName) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public static Staff byUsername(String username) throws SQLException {
		Staff staff = null;
		try (
			PreparedStatement st = DB.connection().prepareStatement("select staff_id, first_name, last_name from staff where username = ?");
		) {
			st.setString(1, username);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				staff = new Staff(rs.getInt(1), rs.getString(2), rs.getString(3));
			}
			rs.close();
			return staff;
		}
	}
	
	public String toString() {
		return id+" - "+firstName+" "+lastName;
	}
}
