package ex3;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Ex3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int i;
		int quantitat;
		Scanner sc = new Scanner(System.in);
		String entrada;
		int total = 0;
		
		// a
		System.out.println("a)");
		Map<String, Integer> compra = new HashMap<String, Integer>();
		String[] items = {"tomàquets", "flams", "pizzes", "llaunes de tonyina", "llaunes de blat de moro", "enciams"};
		int[] quantitats = {6, 4, 2, 2, 5, 1};
		
		for (i=0; i<items.length; i++)
			compra.put(items[i], quantitats[i]);
		
		// b
		System.out.println("b)");
		for (String ingredient : compra.keySet())
			System.out.println(ingredient);

		// c
		System.out.println("c)");
		System.out.println("Tomàquets: "+compra.get("tomàquets"));

		// d
		System.out.println("d)");
		for (String ingredient : compra.keySet()) {
			quantitat = compra.get(ingredient);
		
		    if (quantitat > 3)
		        System.out.println(ingredient);
		}
		
		// e
		System.out.println("e)");
		System.out.println("Digues un ingredient: ");
		entrada = sc.nextLine();
		if (compra.containsKey(entrada))
			System.out.println("Es necessiten "+compra.get(entrada)+" "+entrada);
		else
			System.out.println("Aquest ingredient no és a la llista de la compra.");

		// f
		System.out.println("f)");
		for (String ingredient : compra.keySet()) {
			quantitat = compra.get(ingredient);
			System.out.println(ingredient+" ("+quantitat+")");
		}
		
		// g
		System.out.println("g)");
		for (Integer q : compra.values())
		    total += q;
		System.out.println("Total d'ítems a comprar: "+total);

		// h
		System.out.println("h)");
		System.out.println("Digues un ingredient: ");
		entrada = sc.nextLine();
		System.out.println("Quants "+entrada+ " necessites? ");
		quantitat = sc.nextInt();
		compra.put(entrada, quantitat);
		
		sc.close();
	}

}
