package puntcami;

import java.util.ArrayList;
import java.util.List;

public class Cami {
	private List<Punt> punts = new ArrayList<Punt>();
	
	public boolean afegeixPunt(Punt p) {
		return punts.add(p);
	}
	
	public void afegeixPunt(int index, Punt punt) {
		punts.add(index, punt);
	}
	
	public int mida() {
		return punts.size();
	}
	
	public double distancia() {
		Punt anterior = null;
		double d = 0;
		
		for (Punt actual : punts) {
			if (anterior != null)
				d += anterior.distancia(actual);
			anterior = actual;
		}
		return d;
	}
	
	@Override
	public String toString() {
		String s = "";
		
		for (Punt p : punts)
			s = s+" "+p.toString();
		
		return s;
	}
}
