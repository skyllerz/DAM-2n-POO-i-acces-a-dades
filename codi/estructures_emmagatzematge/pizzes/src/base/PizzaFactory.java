package base;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class PizzaFactory {
	private Map<String, Ingredient> pizzes = new HashMap<String, Ingredient>();
	
	public void addPizza(String nom, Ingredient pizza) {
		pizzes.put(nom.toLowerCase(), pizza);
	}
	
	public Set<String> getNomsPizzes() {
		return Collections.unmodifiableSet(pizzes.keySet());
	}
	
	public Ingredient getPizzaByName(String nom) throws IllegalArgumentException, CloneNotSupportedException {
		if (pizzes.keySet().contains(nom.toLowerCase()))
			return pizzes.get(nom.toLowerCase()).clone();
		throw new IllegalArgumentException("No hi ha cap pizza amb aquest nom");
	}
}
