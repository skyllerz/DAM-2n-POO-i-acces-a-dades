package ingredients;

import base.Ingredient;
import base.IngredientExtra;

public class Tonyina extends IngredientExtra implements Cloneable {
	public Tonyina(Ingredient base) {
		super(base);
	}
	@Override
	public String recepta() {
		String rec = super.recepta() + "\n";
		return rec + "El cuiner obre una llauna de tonyina i l'esmicola per sobre.";
	}
	@Override
	public double getPreu() {
		double preu = super.getPreu();
		return preu + 2.5;
	}
	@Override
	public String descripcio() {
		String desc = super.descripcio()+", ";
		return desc + "tonyina";
	}
	@Override
	public Tonyina clone() throws CloneNotSupportedException {
		return (Tonyina) super.clone();
	}
}
